﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/


namespace SimpleMathematika.Utilites.Validators
{
    public abstract class Validator < ValueType >
    {
        public ValueType Data { get; set; }

/*---------------------------------------------------------------------------*/

        protected Validator ( ValueType _value )
        {
            validateValue( _value );
            Data = _value;
        }

/*---------------------------------------------------------------------------*/

        protected virtual void validateValue ( ValueType _value)
        {
            if ( _value == null )
                throw new System.ArgumentNullException( "No value provided!" );
        }
   }
}


/*****************************************************************************/