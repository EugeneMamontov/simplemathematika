﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using SimpleMathematika.Utilites.Validators;
/*****************************************************************************/


namespace SimpleMathematika.Utilites.Values
{
    public class NonEmptyString : Validator < string >
    {
        public NonEmptyString ( string _value )
            :   base ( _value )
        {
        }

/*---------------------------------------------------------------------------*/

        protected override void validateValue ( string _value )
        {
            base.validateValue( _value );

            if ( _value.Length == 0 )
                throw new System.ArgumentException( "Empty string specified!" );
        }
    }
}


/*****************************************************************************/