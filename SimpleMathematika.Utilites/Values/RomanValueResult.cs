﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using System;
/*****************************************************************************/


namespace SimpleMathematika.Utilites.Values
{
    public class RomanValueResult : MathValue
    {
        public RomanValueResult( double _value )
            :   base ( _value )
        {
        }

/*---------------------------------------------------------------------------*/

        protected override void validateValue( double _value )
        {
            base.validateValue( _value );

            if ( _value < 0 || _value > 3999 )
                throw new ArgumentException( 
                        "Roman Value cannot be less or equal zero and " 
                    +   "greater than 3999!"        
                );
        }
    }
}


/*****************************************************************************/