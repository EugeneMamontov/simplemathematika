﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using System;
using SimpleMathematika.Utilites.Validators;
/*****************************************************************************/


namespace SimpleMathematika.Utilites.Values
{
    public class MathValue : Validator < double >
    {
        public MathValue ( double _value ) 
            :   base ( _value )
        {
        }

/*---------------------------------------------------------------------------*/

        public int toInt ()
        {
            return Convert.ToInt32( Data );
        }

/*---------------------------------------------------------------------------*/

        public static MathValue operator + ( MathValue _v1, MathValue _v2 )
        {
            return new MathValue( _v1.Data + _v2.Data );
        }

/*---------------------------------------------------------------------------*/

        public static MathValue operator - ( MathValue _v1, MathValue _v2 )
        {
            return new MathValue( _v1.Data - _v2.Data );
        }

/*---------------------------------------------------------------------------*/

        public static MathValue operator * ( MathValue _v1, MathValue _v2 )
        {
            return new MathValue( _v1.Data * _v2.Data );
        }

/*---------------------------------------------------------------------------*/

        public static MathValue operator / ( MathValue _v1, MathValue _v2 )
        {
            return new MathValue( _v1.Data / _v2.Data );
        }
    }
}


/*****************************************************************************/