﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using System.Collections.Generic;
using SimpleMathematika.Utilites.Values;
/*****************************************************************************/


namespace SimpleMathematika.Core.Engines
{
    public class RomanCalculatorEngine
    {
        public RomanCalculatorEngine () { }

/*---------------------------------------------------------------------------*/

        public double convertToArabianValue ( string _dataToConvert )
        {
            List< double > tempResultsList = new List< double >();
            double conversionResult = 0.0;

            foreach ( var symbol in _dataToConvert )
            {
                switch ( symbol )
                {
                    case 'M':
                        tempResultsList.Add( 1000 );
                        break;
                    case 'D':
                        tempResultsList.Add( 500 );
                        break;
                    case 'C':
                        tempResultsList.Add( 100 );
                        break;
                    case 'L':
                        tempResultsList.Add( 50 );
                        break;
                    case 'X':
                        tempResultsList.Add( 10 );
                        break;
                    case 'V':
                        tempResultsList.Add( 5 );
                        break;
                    case 'I':
                        tempResultsList.Add( 1 );
                        break;
                    default:
                        tempResultsList.Add( 0 );
                        break;
                }

                int indexOfLast = tempResultsList.Count - 1;
                conversionResult = tempResultsList[ indexOfLast ];
                
                for ( int i = indexOfLast; i >= 1; --i )
                {
                    double current = tempResultsList[ i ];
                    double next = tempResultsList[ i - 1 ];

                    if ( current > next )
                        conversionResult -= next;
                    else
                        conversionResult += next; 
                }
            }

            return conversionResult;
        }

/*---------------------------------------------------------------------------*/

        public string convertToRomanValue ( RomanValueResult _Result )
        {
            string convertResult = "";
            int valueToConvert = _Result.toInt();

            while ( valueToConvert != 0 )
            { 
                checkMainValues(  ref valueToConvert, ref convertResult );
                checkExeptableValues( ref valueToConvert, ref convertResult );
            }

            return convertResult;          
        }

/*---------------------------------------------------------------------------*/

        public bool isCorrectRepeatedDivisibleByTen ( string _data )
        {
            for ( int i = 0; i < _data.Length; ++i )
            { 
                if ( isSymbolRepeatsMoreThan3Times( 'I', i, _data ) )
                    return false;

                if ( isSymbolRepeatsMoreThan3Times( 'X', i, _data ) )
                    return false;

                if ( isSymbolRepeatsMoreThan3Times( 'C', i, _data ) )
                    return false;

                if ( isSymbolRepeatsMoreThan3Times( 'M', i, _data ) )
                    return false;
            }

            return true;
        }

/*---------------------------------------------------------------------------*/

        public bool isRepeatedDivisibleByFive ( string _dataToConvert )
        {
            for ( int i = 0; i < _dataToConvert.Length; ++i )
            {
                if ( isSymbolRepeatsMoreThanOnce( 'V', i, _dataToConvert ) )
                    return true;

                if ( isSymbolRepeatsMoreThanOnce( 'L', i, _dataToConvert ) )
                    return true;

                if ( isSymbolRepeatsMoreThanOnce( 'D', i, _dataToConvert ) )
                    return true;
            }

            return false;
        }

/*---------------------------------------------------------------------------*/

        private bool isSymbolRepeatsMoreThan3Times ( 
                char _symbol
            ,   int _index
            ,   string _dataToConvert 
        )
        {
            return (
                    _dataToConvert.Length > 3
                &&  _dataToConvert[ _index ] == _symbol
                &&  _dataToConvert[ _index ] == _dataToConvert[ _index + 1 ]
                &&  _dataToConvert[ _index ] == _dataToConvert[ _index + 2 ]
                &&  _dataToConvert[ _index ] == _dataToConvert[ _index + 3 ]
            );
        }

/*---------------------------------------------------------------------------*/

        private bool isSymbolRepeatsMoreThanOnce ( 
                char _symbol
            ,   int _index
            ,   string _dataToConvert 
        )
        {
            return (
                    _dataToConvert.Length >= 2
                &&  _dataToConvert[ _index ] == _symbol
                &&  _dataToConvert[ _index ] == _dataToConvert[ _index + 1 ]    
            );
        }

/*---------------------------------------------------------------------------*/

        private void checkMainValues ( ref int _value, ref string _result )
        {
            int countOfLitera = 0;

            countOfLitera = countLocalSymbol( ref _value, 1000, 4000 );
            while ( countOfLitera != 0 )
            {
                _result += "M";
                --countOfLitera;
            }

            countOfLitera = countLocalSymbol( ref _value, 500, 900 );
            while ( countOfLitera != 0 )
            {
                _result += "D";
                --countOfLitera;
            }

            countOfLitera = countLocalSymbol( ref _value, 100, 400 );
            while ( countOfLitera != 0 )
            {
                _result += "C";
                --countOfLitera;
            }

            countOfLitera = countLocalSymbol( ref _value, 50, 90 );
            while ( countOfLitera != 0 )
            {
                _result += "L";
                --countOfLitera;
            }

            countOfLitera = countLocalSymbol( ref _value, 10, 40 );
            while ( countOfLitera != 0 )
            {
                _result += "X";
                --countOfLitera;
            }

            countOfLitera = countLocalSymbol( ref _value, 5, 9 );
            while ( countOfLitera != 0 )
            {
                _result += "V";
                --countOfLitera;
            }

            countOfLitera = countLocalSymbol( ref _value, 1, 4 );
            while ( countOfLitera != 0 )
            {
                _result += "I";
                --countOfLitera;
            }
        }

/*---------------------------------------------------------------------------*/

        private int countLocalSymbol ( 
                ref int _value
            ,   int _leftLimit
            ,   int _rightLimit 
        )
        {
            int countOfLitera = 0;
            
            if ( _value < _rightLimit && _value >= _leftLimit )
            {
                countOfLitera = _value / _leftLimit;
                _value %= _leftLimit;
            }

            return countOfLitera;
        }

/*---------------------------------------------------------------------------*/

        private void checkExeptableValues ( ref int _value, ref string _result )
        {
            if ( _value >= 900 && _value <= 999 )
            { 
                _result += "CM";
                _value -= 900;
            }

            if ( _value >= 400 && _value <= 499 )
            { 
                _result += "CD";
                _value -= 400;
            }

            if ( _value >= 90 && _value <= 99 )
            { 
                _result += "XC";
                _value -= 90;
            }

            if ( _value >= 40 && _value <= 49 )
            { 
                _result += "XL";
                _value -= 40;
            }

            if ( _value == 9 )
            {
                _result += "IX";
                _value -= 9;
            }

            if ( _value == 4 )
            {
                _result += "IV";
                _value -= 4;
            }   
        }
    }
}


/*****************************************************************************/