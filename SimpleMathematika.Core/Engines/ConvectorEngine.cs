﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using System;
/*****************************************************************************/


namespace SimpleMathematika.Core.Engines
{
    public class ConvectorEngine
    {
        public ConvectorEngine () { }

/*---------------------------------------------------------------------------*/

        public void convertFromIntToHex ( ref int _value, ref string _result )
        {
            _result = Convert.ToString( _value, 16 );
        }

/*---------------------------------------------------------------------------*/

        public string convertFromIntToHex ( ref int _value )
        {
            return Convert.ToString( _value, 16 );
        }

/*---------------------------------------------------------------------------*/

        public void convertFromIntToBinary ( 
                ref int _value
            ,   ref string _result 
        )
        {
            _result = Convert.ToString( _value, 2 );
        }

/*---------------------------------------------------------------------------*/

        public string convertFromIntToBinary ( ref int _value )
        {
            return Convert.ToString( _value, 2 );
        }

/*---------------------------------------------------------------------------*/

        public void convertFromIntToOctal ( ref int _value, ref string _result )
        {
            _result = Convert.ToString( _value, 8 );
        }

/*---------------------------------------------------------------------------*/

        public string convertFromIntToOctal ( ref int _value )
        {
            return Convert.ToString( _value, 8 );
        }

/*---------------------------------------------------------------------------*/

        public void convertFromBinaryToInt ( 
                ref string _value
            ,   ref int _result 
        )
        {
            _result = Convert.ToInt32( _value, 2 );
        }

/*---------------------------------------------------------------------------*/

        public int convertFromBinaryToInt ( ref string _value )
        {
            return Convert.ToInt32( _value, 2 );
        }

/*---------------------------------------------------------------------------*/

        public void convertFromHexToInt ( 
                ref string _value
            ,   ref int _result 
        )
        {
            _result = Convert.ToInt32( _value, 16 );
        }

/*---------------------------------------------------------------------------*/

        public int convertFromHexToInt ( ref string _value )
        {
            return Convert.ToInt32( _value, 16 );
        }

/*---------------------------------------------------------------------------*/

        public void convertFromOctalToInt ( 
                ref string _value
            ,   ref int _result 
        )
        {
            _result = Convert.ToInt32( _value, 8 );
        }

/*---------------------------------------------------------------------------*/

        public int convertFromOctalToInt ( ref string _value )
        {
            return Convert.ToInt32( _value, 8 );
        }

/*---------------------------------------------------------------------------*/

        public void convertFromHexToBinary ( 
                ref string _value
            ,   ref string _result 
        )
        {
            int temp = convertFromHexToInt( ref _value );
            _result = convertFromIntToBinary( ref temp );
        }

/*---------------------------------------------------------------------------*/

        public string convertFromHexToBinary ( ref string _value )
        {
            int temp = convertFromHexToInt( ref _value );
            return convertFromIntToBinary( ref temp );
        }

/*---------------------------------------------------------------------------*/

        public void convertFromHexToOctal ( 
                ref string _value
            ,   ref string _result 
        )
        {
            int temp = convertFromHexToInt( ref _value );
            _result = convertFromIntToOctal( ref temp );
        }

/*---------------------------------------------------------------------------*/

        public string convertFromHexToOctal ( ref string _value )
        {
            int temp = convertFromHexToInt( ref _value );
            return convertFromIntToOctal( ref temp );
        }

/*---------------------------------------------------------------------------*/

        public void convertFromOctalToBinary ( 
                ref string _value
            ,   ref string _result 
        )
        {
            int temp = convertFromOctalToInt( ref _value );
            _result = convertFromIntToBinary( ref temp );
        }

/*---------------------------------------------------------------------------*/

        public string convertFromOctalToBinary ( ref string _value )
        {
            int temp = convertFromOctalToInt( ref _value );
            return convertFromIntToBinary( ref temp );
        }

/*---------------------------------------------------------------------------*/

        public void convertFromOctalToHex ( 
                ref string _value
            ,   ref string _result 
        )
        {
            int temp = convertFromOctalToInt( ref _value );
            _result = convertFromIntToHex( ref temp );
        }

/*---------------------------------------------------------------------------*/

        public string convertFromOctalToHex ( ref string _value )
        {
            int temp = convertFromOctalToInt( ref _value );
            return convertFromIntToHex( ref temp );
        }

/*---------------------------------------------------------------------------*/

        public void convertFromBinaryToHex ( 
                ref string _value
            ,   ref string _result 
        )
        {
            int temp = convertFromBinaryToInt( ref _value );
            _result = convertFromIntToHex( ref temp );
        }

/*---------------------------------------------------------------------------*/

        public string convertFromBinaryToHex ( ref string _value )
        {
            int temp = convertFromBinaryToInt( ref _value );
            return convertFromIntToHex( ref temp );
        }

/*---------------------------------------------------------------------------*/

        public void convertFromBinaryToOctal ( 
                ref string _value
            ,   ref string _result 
        )
        {
            int temp = convertFromBinaryToInt( ref _value );
            _result = convertFromIntToOctal( ref temp );
        }

/*---------------------------------------------------------------------------*/

        public string convertFromBinaryToOctal ( ref string _value )
        {
            int temp = convertFromBinaryToInt( ref _value );
            return convertFromIntToOctal( ref temp );
        }
    }
}


/*****************************************************************************/