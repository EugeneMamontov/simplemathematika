﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using System.Collections.Generic;
/*****************************************************************************/

namespace SimpleMathematika.Core.FormulaParser
{
    public class EquatationRoots
    {
        public List< double > Roots { get; set; }
        public string Message { get; set; }

/*---------------------------------------------------------------------------*/

        public EquatationRoots ()
        {
            Roots = new List< double >();
        }

/*---------------------------------------------------------------------------*/

        public EquatationRoots ( string _message )
        {
            Roots = new List< double >();
            Message = _message;
        }

/*---------------------------------------------------------------------------*/

        public EquatationRoots ( double x1, double x2 = 0, string message = "" )
        {
            Roots = new List< double >();
            Roots.Add( x1 );
            Roots.Add( x2 );
        }

/*---------------------------------------------------------------------------*/

        public override string ToString ()
        {
            string res = "";

            if ( Roots.Count == 0 )
                res = "\nDiscriminand less than zero. No roots found.";
            else if ( Roots.Count == 1 )
            {
                res += "\nDiscriminand = 0. Only one root found.";
                res += "X = " + Roots[ 0 ] + " ";
            }
            else
            {
                res += "\nDiscriminand greater than zero. Roots are: ";
                for ( int i = 0; i < Roots.Count; ++i )
                    res += "X" + ( i + 1 ) + " = " + Roots[ i ] + " ";
            }
            return res;
        }
    }
}


/*****************************************************************************/
