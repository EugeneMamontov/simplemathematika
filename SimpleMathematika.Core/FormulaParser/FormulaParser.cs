﻿/*****************************************************************************/
using System;
using System.Linq;
using System.Collections.Generic;
/*****************************************************************************/


namespace SimpleMathematika.Core.FormulaParser
{
    public class FormulaParser
    {
        private Dictionary < string, double > ExpressionMembers { get; set; }

/*---------------------------------------------------------------------------*/

        public FormulaParser ()
        {
            ExpressionMembers = new Dictionary< string, double >();
        }

/*---------------------------------------------------------------------------*/

        public EquatationRoots evaluateSquardEquatation ( string _stringToParse )
        {
            string numbers = getStringWithoutXes( _stringToParse );

            Result a = getNumericValue( numbers );
            Result b = getNumericValue( a.Rest );
            Result c = getNumericValue( b.Rest );

            double aVal = a.AccumulatedValue.Data;
            double bVal = b.AccumulatedValue.Data;
            double cVal = c.AccumulatedValue.Data;

            double discriminant = bVal * bVal - 4 * aVal * cVal;

            if ( discriminant < 0 )
                return new EquatationRoots( "Discriminant lower than zero." );

            else if ( discriminant == 0 )
                return new EquatationRoots ( -bVal / 2 * aVal );
            else
            {
                double x1 = ( ( -bVal - Math.Sqrt( discriminant ) ) / 2 * aVal );
                double x2 = ( ( -bVal + Math.Sqrt( discriminant ) ) / 2 * aVal );
                return new EquatationRoots( x1, x2 );
            }
        }

/*---------------------------------------------------------------------------*/

        private string getStringWithoutXes ( string _stringToParse )
        {
            string result = "";

            for ( int i = 0; i < _stringToParse.Length; ++i )
                if ( 
                        Char.IsDigit( _stringToParse.ElementAt( i ) )
                   ||   _stringToParse.ElementAt( i ) == '+' 
                   ||   _stringToParse.ElementAt( i ) == '-'
                )
                    result += _stringToParse.ElementAt( i );

            return result;
        }

/*---------------------------------------------------------------------------*/

        public double parse ( string _stringToParse )
        {
            Result result = evaluatePlusMinus( _stringToParse );
            return result.AccumulatedValue.Data;
        }

/*---------------------------------------------------------------------------*/

        private Result evaluatePlusMinus ( string _stringToParse )
        {
            Result current = evaluateMulDiv( _stringToParse );
            double acc = current.AccumulatedValue.Data;

            while ( current.Rest.Length > 0)
            {
                char sign = current.Rest.First();

                if ( sign != '+' && sign != '-' )
                    break;

                string next = current.Rest.Substring( 1 );
                current = evaluateMulDiv( next );

                if ( sign == '+' )
                    acc += current.AccumulatedValue.Data;
                else
                    acc -= current.AccumulatedValue.Data;
            }

            return new Result( acc, current.Rest );
        }

/*---------------------------------------------------------------------------*/

        private Result evaluateMulDiv ( string _stringToParse )
        {
            Result current = onBracket( _stringToParse );
            double acc = current.AccumulatedValue.Data;

            while ( current.Rest.Length > 0 )
            {                
                char sign = current.Rest.First();

                if ( sign != '*' && sign != '/' )
                    return current;

                string next = current.Rest.Substring( 1 );
                Result right = onBracket( next );

                if ( sign == '*' ) 
                    acc *= right.AccumulatedValue.Data;
                 else 
                    acc /= right.AccumulatedValue.Data;

                current = new Result( acc, right.Rest );
            }

            return current;
        }

/*---------------------------------------------------------------------------*/

        private Result onBracket ( string _stringToParse )
        {
            char first = _stringToParse.First();

            if ( first == '(' )
            {
                Result result 
                    = evaluatePlusMinus( _stringToParse.Substring( 1 ) );

                if ( 
                        result.Rest.Length != 0 
                    &&  result.Rest.First() == ')' 
                )
                { 
                    result.Rest = result.Rest.Substring( 1 );
                    return result;
                }
                else 
                    throw new Exception ( "Error: No close bracket found!" );
            }

            return onFunctionVariable( _stringToParse );
        }

/*---------------------------------------------------------------------------*/

        private Result onFunctionVariable ( string _stringToParse )
        {
            string funcName = "";
            int count = 0;

            while (
                    count < _stringToParse.Length
                && (
                        Char.IsLetter( _stringToParse.ElementAt( count ) )
                    || ( 
                            Char.IsDigit( _stringToParse.ElementAt( count ) ) 
                        &&  count > 0 )
                        )
                   )
            {
                funcName += _stringToParse.ElementAt( count );
                ++count;
            }

            int fSize = funcName.Length;

            if ( fSize != 0 )
                if ( 
                        _stringToParse.Length > count 
                    &&  _stringToParse.ElementAt( count ) == '('
                )
                {
                    // NOTE: If bracket - it is a function. Otherwise it's 
                    // a simple variable.

                    Result result 
                        = onBracket( _stringToParse.Substring( fSize ) );

                    return processFunction( funcName, result );
                }
                else
                    return new Result(
                            getExpressionVar( funcName )
                        ,   _stringToParse.Substring( fSize ) );

            return getNumericValue( _stringToParse );
        }

/*---------------------------------------------------------------------------*/

        private Result processFunction ( string _funcName, Result _result )
        {
            if ( _funcName.Equals( "sin" ) ) 
                return new Result( 
                        Math.Sin( _result.AccumulatedValue.Data )
                    ,   _result.Rest 
                );
            else if ( _funcName.Equals( "sinH" ) ) 
                return new Result( 
                        Math.Sinh( _result.AccumulatedValue.Data )
                    ,   _result.Rest 
                );

            else if ( _funcName.Equals( "cos" ) )
                return new Result(
                        Math.Cos( _result.AccumulatedValue.Data )
                    ,   _result.Rest
                );
            else if ( _funcName.Equals( "cosH" ) ) 
                return new Result( 
                        Math.Cosh( _result.AccumulatedValue.Data )
                    ,   _result.Rest 
                );

            else if ( _funcName.Equals( "tan" ) ) 
                return new Result( 
                        Math.Tan( _result.AccumulatedValue.Data )
                    ,   _result.Rest 
                );

            else if ( _funcName.Equals( "tanH" ) ) 
                return new Result( 
                        Math.Tanh( _result.AccumulatedValue.Data )
                    ,   _result.Rest 
                );

            else if ( _funcName.Equals( "log" ) ) 
                return new Result( 
                        Math.Log( _result.AccumulatedValue.Data )
                    ,   _result.Rest 
                );

            else 
                throw new Exception ( 
                    String.Format( 
                            "Function '{0}' is not defined!"
                        ,   _funcName 
                    ) 
                );
        }

/*---------------------------------------------------------------------------*/

        public double getExpressionVar ( string _variableName )
        {
            if ( ExpressionMembers.ContainsKey(_variableName) )
                return ExpressionMembers[ _variableName ];

            return 0.0;
        }

/*---------------------------------------------------------------------------*/

        public void addExpressionMember (string _memberName, double _memberValue )
        {
            ExpressionMembers.Add( _memberName, _memberValue );
        }

/*---------------------------------------------------------------------------*/

        private Result getNumericValue ( string _stringToParse )
        {
            char first = _stringToParse.First();
            bool isNegative = checkIsNegative( ref _stringToParse );
            int symbolsToParseCount = 0;

            checkOnDigits( _stringToParse, ref symbolsToParseCount );

            double tempResult 
                = double.Parse( _stringToParse.Substring( 
                                    0
                                ,   symbolsToParseCount 
                                ) 
                        );

            if ( isNegative )
                tempResult = -tempResult;
               
            string restPart = _stringToParse.Substring( symbolsToParseCount );

            return new Result( tempResult, restPart );
        }

/*---------------------------------------------------------------------------*/

        private bool checkIsNegative ( ref string _stringToParse )
        {
            if ( _stringToParse.First() == '-' )
            {
                _stringToParse = _stringToParse.Substring( 1 );
                return true;
            }
            else if ( _stringToParse.First() == '+' )
            {
                _stringToParse = _stringToParse.Substring( 1 );
                return false;
            }

            return false;
        }

/*---------------------------------------------------------------------------*/

        private void checkOnDigits ( 
                string _stringToParse
            ,   ref int _count 
        )
        {
            int dot_cnt = 0;

            while (
                    _count < _stringToParse.Length 
                &&  ( 
                        Char.IsDigit( _stringToParse.ElementAt( _count ) )  
                    ||  _stringToParse.ElementAt( _count ) == '.' ) 
            )
            {
                if ( _stringToParse.ElementAt( _count ) == '.' && ++dot_cnt > 1 )
                    throw new Exception ( 
                        "Error: Too many points '.' in value represented" 
                    );

                _count++;
            }    
        }
    }
}


/*****************************************************************************/