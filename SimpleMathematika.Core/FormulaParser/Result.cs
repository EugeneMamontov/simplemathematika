﻿/*****************************************************************************/
using SimpleMathematika.Utilites.Values;
/*****************************************************************************/


namespace SimpleMathematika.Core.FormulaParser
{
    public class Result
    {
        public MathValue AccumulatedValue { get; set; }
        public string Rest { get; set; }

/*---------------------------------------------------------------------------*/

        public Result ( double _accumulatedValue, string _rest )
        {
            AccumulatedValue = new MathValue( _accumulatedValue );
            Rest =  _rest;
        }
    }
}


/*****************************************************************************/