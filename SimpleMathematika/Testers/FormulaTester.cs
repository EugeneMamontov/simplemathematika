﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using System;
using System.Collections.Generic;
using SimpleMathematika.Model;
using SimpleMathematika.Testers;
using SimpleMathematika.Core.FormulaParser;
/*****************************************************************************/


namespace SimpleMathematika.TestApp.Testers
{
    public class FormulaTester : BaseTester
    {
        public Formula F { get; set; }

/*---------------------------------------------------------------------------*/

        public FormulaTester ()
        {
            F = new Formula();
        }

/*---------------------------------------------------------------------------*/

        public override void runTests()
        {
            Console.WriteLine( "Running Formula Tests...\n" );

            try
            {
                checkSimpleSequence();
                checkStrongSequence();
                checkFuncSequence();
                checkWithXSequence();
                checkSquardEqualitanceSequence();

                Console.WriteLine( 
                    "Finished Formula Tests with exit code 0.\n" 
                );
            }
            catch ( Exception _ex )
            {
                Console.WriteLine( "Something went wrong...\nExit code -1" );
                Console.WriteLine( _ex.Message );
            }
        }

/*---------------------------------------------------------------------------*/

        private void checkSimpleSequence ()
        {
            F.StringToParse = "2+2*2";
            F.tryParse();

            Console.WriteLine( 
                    "\tTest #1: Formula: {0}, Result: {1}"
                ,   F.StringToParse
                ,   F.Result.Data.ToString()
            );

            checkEqual( F.Result.Data, 6.0 );
        }

/*---------------------------------------------------------------------------*/

        private void checkStrongSequence ()
        {
            F.StringToParse = "(100+23213+123123)/7/100";
            F.tryParse();

            Console.WriteLine( 
                    "\tTest #2: Formula: {0}, Result: {1}"
                ,   F.StringToParse
                ,   F.Result.Data.ToString()
            );

            checkEqual( F.Result.Data, 209.194 );
        }

/*---------------------------------------------------------------------------*/

        private void checkFuncSequence ()
        {
            F.StringToParse = "cos(sin(0))";
            F.tryParse();

            Console.WriteLine( 
                    "\tTest #3: Formula: {0}, Result: {1}"
                ,   F.StringToParse
                ,   F.Result.Data.ToString()
            );

            checkEqual( F.Result.Data, 1 );
        }

/*---------------------------------------------------------------------------*/

        private void checkWithXSequence ()
        {
            F.StringToParse = "cos(sin(X))";
            F.addExpressionMember( "X", 0 );
            F.tryParse();

            Console.WriteLine( 
                    "\tTest #4: Formula: {0}, Result: {1}"
                ,   F.StringToParse
                ,   F.Result.Data.ToString()
            );

            checkEqual( F.Result.Data, 1 );
        }

/*---------------------------------------------------------------------------*/

        private void checkSquardEqualitanceSequence ()
        {
            F.StringToParse = "1X+2X+3";

            Console.WriteLine( 
                    "\tTest #5: Formula: {0}, Result: {1}"
                ,   F.StringToParse
                ,   F.evaluateSquardEquatation().Message
            );

            F.StringToParse = "1X-6X+9";
            Console.WriteLine( 
                    "\tTest #6: Formula: {0}, Result: {1}"
                ,   F.StringToParse
                ,   F.evaluateSquardEquatation().Roots[ 0 ]
            );

            F.StringToParse = "1X+4X+2";
            Console.WriteLine( 
                    "\tTest #7: Formula: {0}, Result: x1 = {1}, x2 = {2}"
                ,   F.StringToParse
                ,   F.evaluateSquardEquatation().Roots[ 0 ]
                ,   F.evaluateSquardEquatation().Roots[ 1 ]
            );
        }
    }
}


/*****************************************************************************/