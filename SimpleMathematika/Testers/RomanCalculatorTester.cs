﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using System;
using SimpleMathematika.Testers;
using SimpleMathematika.Utilites.Values;
using SimpleMathematika.Model.Calculators;
/*****************************************************************************/


namespace SimpleMathematika.TestApp.Testers
{
    public class RomanCalculatorTester : BaseTester
    {
        public RomanCalculator RC { get; set; }

/*---------------------------------------------------------------------------*/

        public RomanCalculatorTester ()
        {
            RC = new RomanCalculator();
        }

/*---------------------------------------------------------------------------*/

        public override void runTests ()
        {
            Console.WriteLine( "Running RomanCalculator Tests...\n" );

            try
            {
                checkEmptyValues();
                checkConvertToArabian();
                checkConvertToArabianWithDifficultValues();
                checkConvertToRoman();
                checkConvertToRomanWithDificultValues();
                checkIsCorrectRepeatedDivisibleByTen();

                Console.WriteLine(
                    "Finished RomanCalculator Tests with exit code 0.\n"
                );
            }
            catch ( Exception _ex )
            {
                Console.WriteLine( "Something went wrong...\nExit code -1" );
                Console.WriteLine( _ex.Message );
            }
        }

/*---------------------------------------------------------------------------*/

        public void checkEmptyValues ()
        {
            Console.WriteLine( "\t=====   Check Empty Values   =====" );

            Console.WriteLine( 
                    "\tTest #1: Data: {0}, SecondOperand: {1}"
                ,   RC.Data
                ,   RC.Result.Data
            );

            Console.WriteLine();

            checkEqual( RC.Data, "" );
            checkEqual( RC.Result.Data, 0.0 );
        }

/*---------------------------------------------------------------------------*/

        private void checkConvertToArabian ()
        {
            Console.WriteLine( "\t=====   Check ConvertToArabian   =====" );

            
            /* ---      first case         --- */

            RC.Data = "M";
            RC.convertToArabianValue();
            Console.WriteLine( 
                    "\tTest #1: Converted {0} to {1}"
                ,   RC.Data
                ,   RC.Result.Data
            );
            checkEqual( RC.Data, "M" );
            checkEqual( RC.Result.Data, 1000.0 );

            /* ---      end case           --- */


            /* ---      second case        --- */

            RC.Data = "D";
            RC.convertToArabianValue();
            Console.WriteLine( 
                    "\tTest #2: Converted {0} to {1}"
                ,   RC.Data
                ,   RC.Result.Data
            );
            checkEqual( RC.Data, "D" );
            checkEqual( RC.Result.Data, 500.0 );

            /* ---      end case           --- */


            /* ---      third case         --- */

            RC.Data = "C";
            RC.convertToArabianValue();
            Console.WriteLine( 
                    "\tTest #3: Converted {0} to {1}"
                ,   RC.Data
                ,   RC.Result.Data
            );
            checkEqual( RC.Data, "C" );
            checkEqual( RC.Result.Data, 100.0 );

            /* ---      end case           --- */


            /* ---      fourth case        --- */

            RC.Data = "L";
            RC.convertToArabianValue();
            Console.WriteLine( 
                    "\tTest #4: Converted {0} to {1}"
                ,   RC.Data
                ,   RC.Result.Data
            );
            checkEqual( RC.Data, "L" );
            checkEqual( RC.Result.Data, 50.0 );

            /* ---      end case           --- */


            /* ---      fifth case         --- */

            RC.Data = "X";
            RC.convertToArabianValue();
            Console.WriteLine( 
                    "\tTest #5: Converted {0} to {1}"
                ,   RC.Data
                ,   RC.Result.Data
            );
            checkEqual( RC.Data, "X");
            checkEqual( RC.Result.Data, 10.0 );

            /* ---      end case           --- */


            /* ---      sixth case         --- */

            RC.Data = "V";
            RC.convertToArabianValue();
            Console.WriteLine( 
                    "\tTest #6: Converted {0} to {1}"
                ,   RC.Data
                ,   RC.Result.Data
            );
            checkEqual( RC.Data, "V" );
            checkEqual( RC.Result.Data, 5.0 );

            /* ---      end case           --- */


            /* ---      sevens case        --- */

            RC.Data = "I";
            RC.convertToArabianValue();
            Console.WriteLine( 
                    "\tTest #7: Converted {0} to {1}"
                ,   RC.Data
                ,   RC.Result.Data
            );
            checkEqual( RC.Data, "I" );
            checkEqual( RC.Result.Data, 1.0 );

            /* ---      end case           --- */

            Console.WriteLine();
        }

/*---------------------------------------------------------------------------*/

        private void checkConvertToArabianWithDifficultValues ()
        {
            Console.WriteLine( 
                "\t=====   Check ConvertToArabian With Difficult Values   =====" 
            );

            /* ---      first case         --- */

            RC.Data = "MMCMX";
            RC.convertToArabianValue();
            Console.WriteLine( 
                    "\tTest #1: Converted {0} to {1}"
                ,   RC.Data
                ,   RC.Result.Data
            );
            checkEqual( RC.Data, "MMCMX");
            checkEqual( RC.Result.Data, 2910.0 );

            /* ---      end case           --- */


            /* ---      second case        --- */

            RC.Data = "MMD";
            RC.convertToArabianValue();
            Console.WriteLine( 
                    "\tTest #2: Converted {0} to {1}"
                ,   RC.Data
                ,   RC.Result.Data
            );
            checkEqual( RC.Data, "MMD");
            checkEqual( RC.Result.Data, 2500.0 );

            /* ---      end case           --- */


            /* ---      third case         --- */

            RC.Data = "CDLXXIII";
            RC.convertToArabianValue();
            Console.WriteLine( 
                    "\tTest #3: Converted {0} to {1}"
                ,   RC.Data
                ,   RC.Result.Data
            );
            checkEqual( RC.Data, "CDLXXIII");
            checkEqual( RC.Result.Data, 473.0 );

            /* ---      end case           --- */


            /* ---      fourth case         --- */

            RC.Data = "DCXII";
            RC.convertToArabianValue();
            Console.WriteLine( 
                    "\tTest #4: Converted {0} to {1}"
                ,   RC.Data
                ,   RC.Result.Data
            );
            checkEqual( RC.Data, "DCXII");
            checkEqual( RC.Result.Data, 612.0 );

            /* ---      end case           --- */


            /* ---      fifth case         --- */

            RC.Data = "MDCCCLXXVIII";
            RC.convertToArabianValue();
            Console.WriteLine( 
                    "\tTest #5: Converted {0} to {1}"
                ,   RC.Data
                ,   RC.Result.Data
            );
            checkEqual( RC.Data, "MDCCCLXXVIII");
            checkEqual( RC.Result.Data, 1878.0 );

            /* ---      end case           --- */


            /* ---      sixth case         --- */

            RC.Data = "XVII";
            RC.convertToArabianValue();
            Console.WriteLine( 
                    "\tTest #6: Converted {0} to {1}"
                ,   RC.Data
                ,   RC.Result.Data
            );
            checkEqual( RC.Data, "XVII");
            checkEqual( RC.Result.Data, 17.0 );

            /* ---      end case           --- */


            /* ---      seventh case       --- */

            RC.Data = "IV";
            RC.convertToArabianValue();
            Console.WriteLine( 
                    "\tTest #7: Converted {0} to {1}"
                ,   RC.Data
                ,   RC.Result.Data
            );
            checkEqual( RC.Data, "IV");
            checkEqual( RC.Result.Data, 4.0 );

            /* ---      end case           --- */


            /* ---      eigth case        --- */

            RC.Data = "IX";
            RC.convertToArabianValue();
            Console.WriteLine(
                    "\tTest #8: Converted {0} to {1}"
                , RC.Data
                , RC.Result.Data
            );
            checkEqual( RC.Data, "IX");
            checkEqual( RC.Result.Data, 9.0 );

            /* ---      end case           --- */


            /* ---      nineth case        --- */

            RC.Data = "XC";
            RC.convertToArabianValue();
            Console.WriteLine(
                    "\tTest #9: Converted {0} to {1}"
                , RC.Data
                , RC.Result.Data
            );
            checkEqual( RC.Data, "XC");
            checkEqual( RC.Result.Data, 90.0 );

            /* ---      end case           --- */


            /* ---      tenth case     --- */

            RC.Data = "CD";
            RC.convertToArabianValue();
            Console.WriteLine(
                    "\tTest #10: Converted {0} to {1}"
                , RC.Data
                , RC.Result.Data
            );
            checkEqual( RC.Data, "CD");
            checkEqual( RC.Result.Data, 400.0 );

            /* ---      end case           --- */


            /* ---      eleventh case        --- */

            RC.Data = "CM";
            RC.convertToArabianValue();
            Console.WriteLine(
                    "\tTest #11: Converted {0} to {1}"
                , RC.Data
                , RC.Result.Data
            );
            checkEqual( RC.Data, "CM");
            checkEqual( RC.Result.Data, 900.0 );

            /* ---      end case           --- */

            Console.WriteLine();
        }

/*---------------------------------------------------------------------------*/

        private void checkConvertToRoman ()
        {
            Console.WriteLine( "\t=====   Check ConvertToRoman   =====" );

            /* ---      first case         --- */

            RC.Result = new RomanValueResult( 1000 );
            Console.WriteLine( 
                    "\tTest #1: Converted {0} to {1}"
                ,   RC.Result.Data
                ,   RC.convertToRomanValue()
            );
            checkEqual( RC.convertToRomanValue(), "M" );
            checkEqual( RC.Result.Data, 1000.0 );

            /* ---      end case           --- */


            /* ---      second case         --- */

            RC.Result = new RomanValueResult( 500 );
            Console.WriteLine( 
                    "\tTest #2: Converted {0} to {1}"
                ,   RC.Result.Data
                ,   RC.convertToRomanValue()
            );
            checkEqual( RC.convertToRomanValue(), "D" );
            checkEqual( RC.Result.Data, 500.0 );

            /* ---      end case           --- */


            /* ---      third case         --- */

            RC.Result = new RomanValueResult( 100 );
            Console.WriteLine( 
                    "\tTest #3: Converted {0} to {1}"
                ,   RC.Result.Data
                ,   RC.convertToRomanValue()
            );
            checkEqual( RC.convertToRomanValue(), "C" );
            checkEqual( RC.Result.Data, 100.0 );

            /* ---      end case           --- */


            /* ---      fourth case        --- */

            RC.Result = new RomanValueResult( 50 );
            Console.WriteLine( 
                    "\tTest #4: Converted {0} to {1}"
                ,   RC.Result.Data
                ,   RC.convertToRomanValue()
            );
            checkEqual( RC.convertToRomanValue(), "L" );
            checkEqual( RC.Result.Data, 50.0 );

            /* ---      end case           --- */


            /* ---      fifth case         --- */

            RC.Result = new RomanValueResult( 10 );
            Console.WriteLine( 
                    "\tTest #5: Converted {0} to {1}"
                ,   RC.Result.Data
                ,   RC.convertToRomanValue()
            );
            checkEqual( RC.convertToRomanValue(), "X" );
            checkEqual( RC.Result.Data, 10.0 );

            /* ---      end case           --- */


            /* ---      sixth case         --- */

            RC.Result = new RomanValueResult( 5 );
            Console.WriteLine( 
                    "\tTest #6: Converted {0} to {1}"
                ,   RC.Result.Data
                ,   RC.convertToRomanValue()
            );
            checkEqual( RC.convertToRomanValue(), "V" );
            checkEqual( RC.Result.Data, 5.0 );

            /* ---      end case           --- */


            /* ---      seventh case         --- */

            RC.Result = new RomanValueResult( 1 );
            Console.WriteLine( 
                    "\tTest #7: Converted {0} to {1}"
                ,   RC.Result.Data
                ,   RC.convertToRomanValue()
            );
            checkEqual( RC.convertToRomanValue(), "I" );
            checkEqual( RC.Result.Data, 1.0 );

            /* ---      end case           --- */

            Console.WriteLine();
        }

/*---------------------------------------------------------------------------*/

        private void checkConvertToRomanWithDificultValues ()
        {
            Console.WriteLine( 
                "\t=====   Check ConvertToRoman With Difficult Values   =====" 
            );

            /* ---      first case         --- */

            RC.Result = new RomanValueResult( 917 );
            Console.WriteLine( 
                    "\tTest #1: Converted {0} to {1}"
                ,   RC.Result.Data
                ,   RC.convertToRomanValue()
            );
            checkEqual( RC.convertToRomanValue(), "CMXVII" );
            checkEqual( RC.Result.Data, 917.0 );

            /* ---      end case           --- */


            /* ---      second case         --- */

            RC.Result = new RomanValueResult( 574 );
            Console.WriteLine( 
                    "\tTest #2: Converted {0} to {1}"
                ,   RC.Result.Data
                ,   RC.convertToRomanValue()
            );
            checkEqual( RC.convertToRomanValue(), "DLXXIV" );
            checkEqual( RC.Result.Data, 574.0 );

            /* ---      end case           --- */


            /* ---      third case         --- */

            RC.Result = new RomanValueResult( 127 );
            Console.WriteLine( 
                    "\tTest #3: Converted {0} to {1}"
                ,   RC.Result.Data
                ,   RC.convertToRomanValue()
            );
            checkEqual( RC.convertToRomanValue(), "CXXVII" );
            checkEqual( RC.Result.Data, 127.0 );

            /* ---      end case           --- */


            /* ---      fourth case         --- */

            RC.Result = new RomanValueResult( 69 );
            Console.WriteLine( 
                    "\tTest #4: Converted {0} to {1}"
                ,   RC.Result.Data
                ,   RC.convertToRomanValue()
            );
            checkEqual( RC.convertToRomanValue(), "LXIX" );
            checkEqual( RC.Result.Data, 69.0 );

            /* ---      end case           --- */


            /* ---      fifth case         --- */

            RC.Result = new RomanValueResult( 19 );
            Console.WriteLine( 
                    "\tTest #5: Converted {0} to {1}"
                ,   RC.Result.Data
                ,   RC.convertToRomanValue()
            );
            checkEqual( RC.convertToRomanValue(), "XIX" );
            checkEqual( RC.Result.Data, 19.0 );

            /* ---      end case           --- */


            /* ---      sixth case         --- */

            RC.Result = new RomanValueResult( 133 );
            Console.WriteLine( 
                    "\tTest #6: Converted {0} to {1}"
                ,   RC.Result.Data
                ,   RC.convertToRomanValue()
            );
            checkEqual( RC.convertToRomanValue(), "CXXXIII" );
            checkEqual( RC.Result.Data, 133.0 );

            /* ---      end case           --- */


            /* ---      seventh case         --- */

            RC.Result = new RomanValueResult( 2869 );
            Console.WriteLine( 
                    "\tTest #7: Converted {0} to {1}"
                ,   RC.Result.Data
                ,   RC.convertToRomanValue()
            );
            checkEqual( RC.convertToRomanValue(), "MMDCCCLXIX" );
            checkEqual( RC.Result.Data, 2869.0 );

            /* ---      end case           --- */


            /* ---      eigth case         --- */

            RC.Result = new RomanValueResult( 49 );
            Console.WriteLine( 
                    "\tTest #8: Converted {0} to {1}"
                ,   RC.Result.Data
                ,   RC.convertToRomanValue()
            );
            checkEqual( RC.convertToRomanValue(), "XLIX" );
            checkEqual( RC.Result.Data, 49.0 );

            /* ---      end case            --- */


            /* ---      nineth case         --- */

            RC.Result = new RomanValueResult( 92 );
            Console.WriteLine( 
                    "\tTest #9: Converted {0} to {1}"
                ,   RC.Result.Data
                ,   RC.convertToRomanValue()
            );
            checkEqual( RC.convertToRomanValue(), "XCII" );
            checkEqual( RC.Result.Data, 92.0 );

            /* ---      end case           --- */


            /* ---      tenth case         --- */

            RC.Result = new RomanValueResult( 3001 );
            Console.WriteLine( 
                    "\tTest #10: Converted {0} to {1}"
                ,   RC.Result.Data
                ,   RC.convertToRomanValue()
            );
            checkEqual( RC.convertToRomanValue(), "MMMI" );
            checkEqual( RC.Result.Data, 3001.0 );

            /* ---      end case           --- */


            /* ---      eleventh case      --- */

            RC.Result = new RomanValueResult( 3999 );
            Console.WriteLine( 
                    "\tTest #10: Converted {0} to {1}"
                ,   RC.Result.Data
                ,   RC.convertToRomanValue()
            );
            checkEqual( RC.convertToRomanValue(), "MMMCMXCIX" );
            checkEqual( RC.Result.Data, 3999.0 );

            /* ---      end case           --- */

            Console.WriteLine();
        }

/*---------------------------------------------------------------------------*/

        private void checkIsCorrectRepeatedDivisibleByTen()
        {
            Console.WriteLine(
                "\t=====   Check Is Correct Repeated Divisible By Ten   ====="
            );

            RC.Data = "III";
            Console.WriteLine(
                    "\tTest #1: Is {0} correct repeated: {1}"
                ,   RC.Data
                ,   RC.isCorrectRepeatedDivisibleByTen().ToString()
            );

            RC.Data = "IIII";
            Console.WriteLine(
                    "\tTest #2: Is {0} correct repeated: {1}"
                ,   RC.Data
                ,   RC.isCorrectRepeatedDivisibleByTen().ToString()
            );

            RC.Data = "IIIII";
            Console.WriteLine(
                    "\tTest #3: Is {0} correct repeated: {1}"
                ,   RC.Data
                ,   RC.isCorrectRepeatedDivisibleByTen().ToString()
            );

            RC.Data = "XXX";
            Console.WriteLine(
                    "\tTest #4: Is {0} correct repeated: {1}"
                ,   RC.Data
                ,   RC.isCorrectRepeatedDivisibleByTen().ToString()
            );

            RC.Data = "XXXX";
            Console.WriteLine(
                    "\tTest #5: Is {0} correct repeated: {1}"
                ,   RC.Data
                ,   RC.isCorrectRepeatedDivisibleByTen().ToString()
            );

            RC.Data = "CCC";
            Console.WriteLine(
                    "\tTest #6: Is {0} correct repeated: {1}"
                ,   RC.Data
                ,   RC.isCorrectRepeatedDivisibleByTen().ToString()
            );

            RC.Data = "CCCC";
            Console.WriteLine(
                    "\tTest #7: Is {0} correct repeated: {1}"
                ,   RC.Data
                ,   RC.isCorrectRepeatedDivisibleByTen().ToString()
            );

            RC.Data = "MMM";
            Console.WriteLine(
                    "\tTest #7: Is {0} correct repeated: {1}"
                ,   RC.Data
                ,   RC.isCorrectRepeatedDivisibleByTen().ToString()
            );

            RC.Data = "MMMM";
            Console.WriteLine(
                    "\tTest #8: Is {0} correct repeated: {1}"
                ,   RC.Data
                ,   RC.isCorrectRepeatedDivisibleByTen().ToString()
            );

            RC.Data = "LL";
            Console.WriteLine(
                    "\tTest #9: Is {0} repeated more than once: {1}"
                ,   RC.Data
                ,   RC.isRepeatedDivisibleByFive().ToString()
            );

            Console.WriteLine();
        }
    }
}


/*****************************************************************************/