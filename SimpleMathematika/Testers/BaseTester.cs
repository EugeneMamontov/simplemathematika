﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using System;
/*****************************************************************************/


namespace SimpleMathematika.Testers
{
    public abstract class BaseTester
    {
        public virtual void runTests () { }

/*---------------------------------------------------------------------------*/

        public void checkEqual ( double _result, double _standard )
        {
            if ( ! equalDoubles( _result, _standard ) )
                throw new Exception( 
                    string.Format( 
                            "Checking on equal doubles failed! "
                        +   "{0} != {1}."
                        ,   _result
                        ,   _standard 
                    )
                );
        }

/*---------------------------------------------------------------------------*/

        public void checkEqual ( string _result, string _standard )
        {
            if ( _result != _standard )
                throw new Exception( 
                    string.Format(
                            "Checking on equal strings failed! "
                        +   "{0} != {1}."
                        ,   _result
                        ,   _standard 
                    )
                );
        }

/*---------------------------------------------------------------------------*/

        private bool equalDoubles ( 
                double _d1
            ,   double _d2
            ,   double _allowedDifference = 0.001 
        )
        {
            return System.Math.Abs( _d1 - _d2 ) <= _allowedDifference;
        }
    }
}


/*****************************************************************************/