﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using System;
using SimpleMathematika.Testers;
using SimpleMathematika.Utilites.Values;
using SimpleMathematika.Model.Calculators;
/*****************************************************************************/


namespace SimpleMathematika.TestApp.Testers
{
    public class SimpleCalculatorTester : BaseTester
    {
        public SimpleCalculator SC { get; set; }

/*---------------------------------------------------------------------------*/

        public SimpleCalculatorTester ()
        {
            SC = new SimpleCalculator();
        }

/*---------------------------------------------------------------------------*/

        public override void runTests ()
        {
            Console.WriteLine( "Running SimpleCalculator Tests...\n" );

            try
            {
                checkEmptyValues();
                checkSum();
                checkDifference();
                checkMultiplication();
                checkDivision();

                Console.WriteLine( 
                    "Finished SimpleCalculator Tests with exit code 0.\n" 
                );
            }
            catch ( Exception _ex )
            {
                Console.WriteLine( "Something went wrong...\nExit code -1" );
                Console.WriteLine( _ex.Message );
            }
        }


/*---------------------------------------------------------------------------*/

        private void checkEmptyValues ()
        {
            Console.WriteLine( "\t=====   Check Empty Values   =====" );

            Console.WriteLine( 
                    "\tTest #1: FirstOperand: {0}, SecondOperand: {1}, "
                +   "Result {2}"
                ,   SC.FirstOperand.Data
                ,   SC.SecondOperand.Data
                ,   SC.Result.Data
            );

            checkEqual( SC.FirstOperand.Data, 0.0 );
            checkEqual( SC.SecondOperand.Data, 0.0 );
            checkEqual( SC.Result.Data, 0.0 );

            Console.WriteLine();
        }


/*---------------------------------------------------------------------------*/

        private void checkSum ()
        {
            Console.WriteLine( "\t=====   Check Sum   =====" );

            SC.FirstOperand = new MathValue ( 13.0 );
            SC.SecondOperand = new MathValue ( 27.0 );
            SC.evaluateSum();

            Console.WriteLine( 
                    "\tTest #1: {0} + {1} = {2}"
                ,   SC.FirstOperand.Data
                ,   SC.SecondOperand.Data
                ,   SC.Result.Data
            );

            checkEqual( SC.Result.Data, 40.0 );


            SC.FirstOperand = new MathValue ( 121313.0 );
            SC.SecondOperand = new MathValue ( 21231.43345 );
            SC.evaluateSum();

            Console.WriteLine( 
                    "\tTest #2: {0} + {1} = {2}"
                ,   SC.FirstOperand.Data
                ,   SC.SecondOperand.Data
                ,   SC.Result.Data
            );

            checkEqual( SC.Result.Data, 142544.433 );

            Console.WriteLine();
        }

/*---------------------------------------------------------------------------*/

        private void checkDifference ()
        {
            Console.WriteLine( "\t=====   Check Difference   =====" );

            SC.FirstOperand = new MathValue ( 13.0 );
            SC.SecondOperand = new MathValue ( 27.0 );
            SC.evaluateDifference();

            Console.WriteLine( 
                    "\tTest #1: {0} - {1} = {2}"
                ,   SC.FirstOperand.Data
                ,   SC.SecondOperand.Data
                ,   SC.Result.Data
            );

            checkEqual( SC.Result.Data, -14.0 );

            SC.FirstOperand = new MathValue ( 121313.0 );
            SC.SecondOperand = new MathValue ( 21231.43345 );
            SC.evaluateDifference();

            Console.WriteLine( 
                    "\tTest #2: {0} - {1} = {2}"
                ,   SC.FirstOperand.Data
                ,   SC.SecondOperand.Data
                ,   SC.Result.Data
            );

            checkEqual( SC.Result.Data, 100081.566 );

            Console.WriteLine();
        }

/*---------------------------------------------------------------------------*/

    private void checkMultiplication ()
        {
            Console.WriteLine( "\t=====   Check Multiplication   =====" );

            SC.FirstOperand = new MathValue ( 13.0 );
            SC.SecondOperand = new MathValue ( 27.0 );
            SC.evaluateMultiplication();

            Console.WriteLine( 
                    "\tTest #1: {0} * {1} = {2}"
                ,   SC.FirstOperand.Data
                ,   SC.SecondOperand.Data
                ,   SC.Result.Data
            );

            checkEqual( SC.Result.Data, 351.0 );

            SC.FirstOperand = new MathValue ( 121313.0 );
            SC.SecondOperand = new MathValue ( 21231.43345 );
            SC.evaluateMultiplication();

            Console.WriteLine( 
                    "\tTest #2: {0} * {1} = {2}"
                ,   SC.FirstOperand.Data
                ,   SC.SecondOperand.Data
                ,   SC.Result.Data
            );

            checkEqual( SC.Result.Data, 2575648886.119 );

            Console.WriteLine();
        }

/*---------------------------------------------------------------------------*/

        private void checkDivision ()
        {
            Console.WriteLine( "\t=====   Check Division   =====" );

            SC.FirstOperand = new MathValue ( 13.0 );
            SC.SecondOperand = new MathValue ( 27.0 );
            SC.evaluateDivision();

            Console.WriteLine( 
                    "\tTest #1: {0} / {1} = {2}"
                ,   SC.FirstOperand.Data
                ,   SC.SecondOperand.Data
                ,   SC.Result.Data
            );

            checkEqual( SC.Result.Data, 0.481 );

            SC.FirstOperand = new MathValue ( 121313.0 );
            SC.SecondOperand = new MathValue ( 21231.43345 );
            SC.evaluateDivision();

            Console.WriteLine( 
                    "\tTest #2: {0} / {1} = {2}"
                ,   SC.FirstOperand.Data
                ,   SC.SecondOperand.Data
                ,   SC.Result.Data
            );

            checkEqual( SC.Result.Data, 5.713 );

            Console.WriteLine();
        }
    }
}


/*****************************************************************************/