﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using System;
using SimpleMathematika.Testers;
using SimpleMathematika.Utilites.Values;
using SimpleMathematika.Model.Calculators;
/*****************************************************************************/


namespace SimpleMathematika.TestApp.Testers
{
    public class EngineerCalculatorTester : BaseTester
    {
        public EngineerCalculator EC { get; set; }

/*---------------------------------------------------------------------------*/

        public EngineerCalculatorTester ()
        {
            EC = new EngineerCalculator();
        }

/*---------------------------------------------------------------------------*/

        public override void runTests ()
        {
            Console.WriteLine( "Running Engineer Calculator Tests...\n" );

            try
            {
                checkEmptyValues();

                checkSin();
                checkCos();
                checkTan();
                checkPower();
                checkPow();

                checkConvertFromIntToHex();
                checkConvertFromIntToBinary();
                checkConvertFromIntToOctal();

                Console.WriteLine( 
                    "Finished Engineer Calculator Tests with exit code 0.\n" 
                );
            }
            catch ( Exception _ex )
            {
                Console.WriteLine( "Something went wrong...\nExit code -1" );
                Console.WriteLine( _ex.Message );
            }
        }

/*---------------------------------------------------------------------------*/

        private void checkEmptyValues ()
        {
            Console.WriteLine( "\t=====   Check Empty Values   =====" );

            Console.WriteLine( 
                    "\tTest #1: FirstOperand: {0}, SecondOperand: {1}, "
                +   "Angle: {2}, Result: {3}"
                ,   EC.FirstOperand.Data
                ,   EC.SecondOperand.Data
                ,   EC.Angle.Data
                ,   EC.Result.Data
            );

            Console.WriteLine();

            checkEqual( EC.FirstOperand.Data, 0.0 );
            checkEqual( EC.SecondOperand.Data, 0.0 );
            checkEqual( EC.Result.Data, 0.0 );
        }

/*---------------------------------------------------------------------------*/

        private void checkSin ()
        {
            Console.WriteLine( "\t=====   Check Sin   =====" );

            EC.Angle = new MathValue( 0.0 );
            EC.evaluateSin();

            Console.WriteLine(
                    "\tTest #1: sin({0}) = {1}"
                ,   EC.Angle.Data
                ,   EC.Result.Data
            );

            checkEqual( EC.Result.Data, 0.0 );

            EC.Angle = new MathValue( 1.0 );
            EC.evaluateSin();

            Console.WriteLine(
                    "\tTest #2: sin({0}) = {1}"
                , EC.Angle.Data
                , EC.Result.Data
            );

            checkEqual( EC.Result.Data, 0.841 );

            Console.WriteLine();
        }

/*---------------------------------------------------------------------------*/

        private void checkCos ()
        {
            Console.WriteLine( "\t=====   Check Cos   =====" );

            EC.Angle = new MathValue( 1.0 );
            EC.evaluateCos();

            Console.WriteLine(
                    "\tTest #1: cos({0}) = {1}"
                ,   EC.Angle.Data
                ,   EC.Result.Data
            );

            checkEqual( EC.Result.Data, 0.540 );

            EC.Angle = new MathValue( 0.0 );
            EC.evaluateCos();

            Console.WriteLine(
                    "\tTest #2: cos({0}) = {1}"
                ,   EC.Angle.Data
                ,   EC.Result.Data
            );

            checkEqual( EC.Result.Data, 1.0 );

            Console.WriteLine();
        }

/*---------------------------------------------------------------------------*/

        private void checkTan ()
        {
            Console.WriteLine( "\t=====   Check Tan   =====" );

            EC.Angle = new MathValue( 1.5 );
            EC.evaluateTan();

            Console.WriteLine(
                    "\tTest #1: tan({0}) = {1}"
                ,   EC.Angle.Data
                ,   EC.Result.Data
            );

            checkEqual( EC.Result.Data, 0.070 );

            EC.Angle = new MathValue( 0 );
            EC.evaluateTan();

            Console.WriteLine(
                    "\tTest #2: tan({0}) = {1}"
                ,   EC.Angle.Data
                ,   EC.Result.Data
            );

            checkEqual( EC.Result.Data, 1.0 );

            Console.WriteLine();
        }

/*---------------------------------------------------------------------------*/

        private void checkPower ()
        {
            Console.WriteLine( "\t=====   Check Power   =====" );

            EC.FirstOperand = new MathValue( 2.0 );
            EC.SecondOperand = new MathValue( 5.0 );
            EC.evaluatePower();

            Console.WriteLine(
                    "\tTest #1: {0}^{1} = {2}"
                ,   EC.FirstOperand.Data
                ,   EC.SecondOperand.Data
                ,   EC.Result.Data
            );

            checkEqual( EC.Result.Data, 32 );

            EC.FirstOperand = new MathValue( 12.0 );
            EC.SecondOperand = new MathValue( 2.0 );
            EC.evaluatePower();

            Console.WriteLine(
                    "\tTest #1: {0}^{1} = {2}"
                ,   EC.FirstOperand.Data
                ,   EC.SecondOperand.Data
                ,   EC.Result.Data
            );

            checkEqual( EC.Result.Data, 144 );

            Console.WriteLine();
        }

/*---------------------------------------------------------------------------*/

        private void checkPow ()
        {
            Console.WriteLine( "\t=====   Check Pow   =====" );

            EC.FirstOperand = new MathValue( 4.0 );
            EC.SecondOperand = new MathValue( 2.0 );
            EC.evaluatePow();

            Console.WriteLine(
                    "\tTest #1: {0}^1/{1} = {2}"
                ,   EC.FirstOperand.Data
                ,   EC.SecondOperand.Data
                ,   EC.Result.Data
            );

            checkEqual( EC.Result.Data, 2 );

            EC.FirstOperand = new MathValue( 625.0 );
            EC.SecondOperand = new MathValue( 2.0 );
            EC.evaluatePow();

            Console.WriteLine(
                    "\tTest #1: {0}^1/{1} = {2}"
                ,   EC.FirstOperand.Data
                ,   EC.SecondOperand.Data
                ,   EC.Result.Data
            );

            checkEqual( EC.Result.Data, 25 );

            Console.WriteLine();
        }

/*---------------------------------------------------------------------------*/

        private void checkConvertFromIntToHex ()
        {
            Console.WriteLine( 
                "\t=====   Check Convert From Int to Hex   =====" 
            );

            int value1 = 1;
            Console.WriteLine(
                    "\tTest #1: Convert {0} to {1}"    
                ,   value1
                ,   EC.Engine.convertFromIntToHex( ref value1 )
            );

            int value2 = 10;
            Console.WriteLine(
                    "\tTest #2: Convert {0} to {1}"    
                ,   value2
                ,   EC.Engine.convertFromIntToHex( ref value2 )
            );

            int value3 = 13;
            Console.WriteLine(
                    "\tTest #3: Convert {0} to {1}"    
                ,   value3
                ,   EC.Engine.convertFromIntToHex( ref value3 )
            );

            int value4 = 32;
            Console.WriteLine(
                    "\tTest #4: Convert {0} to {1}"    
                ,   value4
                ,   EC.Engine.convertFromIntToHex( ref value4 )
            );

            int value5 = 27;
            Console.WriteLine(
                    "\tTest #5: Convert {0} to {1}"    
                ,   value5
                ,   EC.Engine.convertFromIntToHex( ref value5 )
            );

            Console.WriteLine();
        }

/*---------------------------------------------------------------------------*/

        private void checkConvertFromIntToBinary ()
        {
            Console.WriteLine( 
                "\t=====   Check Convert From Int to Binary   =====" 
            );

            int value1 = 1;
            Console.WriteLine(
                    "\tTest #1: Convert {0} to {1}"    
                ,   value1
                ,   EC.Engine.convertFromIntToBinary( ref value1 )
            );

            int value2 = 5;
            Console.WriteLine(
                    "\tTest #2: Convert {0} to {1}"    
                ,   value2
                ,   EC.Engine.convertFromIntToBinary( ref value2 )
            );

            int value3 = 8;
            Console.WriteLine(
                    "\tTest #3: Convert {0} to {1}"    
                ,   value3
                ,   EC.Engine.convertFromIntToBinary( ref value3 )
            );

            int value4 = 32;
            Console.WriteLine(
                    "\tTest #4: Convert {0} to {1}"    
                ,   value4
                ,   EC.Engine.convertFromIntToBinary( ref value4 )
            );

            int value5 = 27;
            Console.WriteLine(
                    "\tTest #5: Convert {0} to {1}"    
                ,   value5
                ,   EC.Engine.convertFromIntToBinary( ref value5 )
            );

            Console.WriteLine();
        }

/*---------------------------------------------------------------------------*/

        private void checkConvertFromIntToOctal ()
        {
            Console.WriteLine( 
                "\t=====   Check Convert From Int to Octal   =====" 
            );

            int value1 = 1;
            Console.WriteLine(
                    "\tTest #1: Convert {0} to {1}"    
                ,   value1
                ,   EC.Engine.convertFromIntToOctal( ref value1 )
            );

            int value2 = 5;
            Console.WriteLine(
                    "\tTest #2: Convert {0} to {1}"    
                ,   value2
                ,   EC.Engine.convertFromIntToOctal( ref value2 )
            );

            int value3 = 8;
            Console.WriteLine(
                    "\tTest #3: Convert {0} to {1}"    
                ,   value3
                ,   EC.Engine.convertFromIntToOctal( ref value3 )
            );

            int value4 = 32;
            Console.WriteLine(
                    "\tTest #4: Convert {0} to {1}"    
                ,   value4
                ,   EC.Engine.convertFromIntToOctal( ref value4 )
            );

            int value5 = 27;
            Console.WriteLine(
                    "\tTest #5: Convert {0} to {1}"    
                ,   value5
                ,   EC.Engine.convertFromIntToOctal( ref value5 )
            );

            Console.WriteLine();
        }
    }
}


/*****************************************************************************/