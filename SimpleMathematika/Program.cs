﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using SimpleMathematika.TestApp.Testers;
/*****************************************************************************/


namespace SimpleMathematika.TestApp
{
    public class Program
    {
        static void Main( string[] args )
        {
            SimpleCalculatorTester  SCTester    = new SimpleCalculatorTester();
            EngineerCalculatorTester ECTester   = new EngineerCalculatorTester();
            RomanCalculatorTester    RCTester   = new RomanCalculatorTester();
            FormulaTester F                     = new FormulaTester();

            SCTester.runTests();
            ECTester.runTests();
            RCTester.runTests();
            F.runTests();
        }
    }
}


/*****************************************************************************/