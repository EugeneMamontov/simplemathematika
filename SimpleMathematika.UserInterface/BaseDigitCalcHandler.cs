﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using System;
using System.Windows.Forms;
using System.Collections.Generic;
using SimpleMathematika.Utilites.Values;
using SimpleMathematika.Model.Calculators;
/*****************************************************************************/


namespace SimpleMathematika.UserInterface
{
    public class BaseDigitCalcHandler
    {
        public string Symbol            { get; set; }
        public List < string > Values   { get; set; }

/*---------------------------------------------------------------------------*/

        public BaseDigitCalcHandler ()
        {
            Symbol = "";
            Values = new List< string >();
        }

/*---------------------------------------------------------------------------*/

        public void insertValue ( 
                int _value
            ,   ref RichTextBox _textBox 
        )
        {
            Values.Add( _value.ToString() );
            _textBox.Text += _value.ToString();
        }

 /*---------------------------------------------------------------------------*/

        public virtual void onOperation ( 
                string _operationKind
            ,   Calculator _calc
            ,   ref RichTextBox _tBox
        )
        {
            Symbol = _operationKind;
            _calc.FirstOperand = new MathValue( Convert.ToDouble( Values[ 0 ] ) );
            _tBox.Text += _operationKind.ToString();
        }

/*---------------------------------------------------------------------------*/

        public virtual void clear ()
        {
            Values.Clear();
            Symbol = "";
        }

/*---------------------------------------------------------------------------*/

        public void handleCE (
                Calculator _calc
            ,   ref RichTextBox _tBox
        )
        {
            _calc.FirstOperand = new MathValue( 0.0 );
            _calc.SecondOperand = new MathValue( 0.0 );
            _tBox.Text = "";
        }

/*---------------------------------------------------------------------------*/

        public void handleClear (
                Calculator _calc
            ,   ref RichTextBox _tBox
        )
        {
            _calc.FirstOperand = new MathValue( 0.0 );
            _calc.SecondOperand = new MathValue( 0.0 );
            _tBox.Text = "";
        }

/*---------------------------------------------------------------------------*/

        public void handleErase ( ref RichTextBox _tBox )
        {
            Values.RemoveAt( Values.Count - 1 ); 
            _tBox.Text = _tBox.Text.Remove( _tBox.Text.Length - 1 );
        }

/*---------------------------------------------------------------------------*/

        public void handlePoint ( ref RichTextBox _tBox )
        {
            Values.Add( "," );
            _tBox.Text += ",";
        }
    }
}


/*****************************************************************************/