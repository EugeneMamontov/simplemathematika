﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using System;
using System.Windows.Forms;
using SimpleMathematika.Utilites.Values;
using SimpleMathematika.Model.Calculators;
/*****************************************************************************/


namespace SimpleMathematika.UserInterface.ProjectForms
{
    public partial class SimpleCalcForm : Form
    {
        public SimpleCalculator SC          { get; private set; }
        public BaseDigitCalcHandler Handler { get; private set; }

/*---------------------------------------------------------------------------*/

        public SimpleCalcForm ()
        {
            SC      = new SimpleCalculator();
            Handler = new BaseDigitCalcHandler();
            InitializeComponent();
        }

/*---------------------------------------------------------------------------*/

        private void SimpleCalcForm_Closing ( 
                object sender
            ,   FormClosingEventArgs e 
        )
        {
            Application.Exit();
        }

/*---------------------------------------------------------------------------*/

        private void engineerCalculatorToolStripMenuItem_Click ( 
                object sender
            ,   EventArgs e
        )
        {
            var ECF = new EngineerCalcForm();
            ECF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void romanCalculatorToolStripMenuItem_Click ( 
                object sender
            ,   EventArgs e
        )
        {
            var RCF = new RomanCalcForm();
            RCF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void formulaToolStripMenuItem_Click ( 
                object sender
            ,   EventArgs e 
        )
        {
            var FF = new FormulaForm();
            FF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void graphicaToolStripMenuItem_Click ( 
                object sender
            ,   EventArgs e 
        )
        {
            var GF = new GraphicaForm();
            GF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void converterToolStripMenuItem_Click ( 
                object sender
            ,   EventArgs e
        )
        {
            var C = new ConverterForm();
            C.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void authorToolStripMenuItem_Click ( object sender, EventArgs e )
        {
            MessageBox.Show( 
                    "Invented by Yevheii Mamontov, NURE 2017." 
                +   "\nFor contact use yevhenii.mamontov@nure.ua" 
            );
        }

/*---------------------------------------------------------------------------*/

        private void CEButton_Click ( object sender, EventArgs e )
        {
            Handler.handleCE( SC, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void ClearButton_Click ( object sender, EventArgs e )
        {
            Handler.handleClear( SC, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void EraseButton_Click ( object sender, EventArgs e )
        {
            Handler.handleErase( ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void OneButton_Click ( object sender, EventArgs e )
        {
            Handler.insertValue( 1, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void TwoButton_Click ( object sender, EventArgs e )
        {
            Handler.insertValue( 2, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void ThreeButton_Click ( object sender, EventArgs e )
        {
            Handler.insertValue( 3, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void FourButton_Click ( object sender, EventArgs e )
        {
            Handler.insertValue( 4, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void FiveButton_Click ( object sender, EventArgs e )
        {
            Handler.insertValue( 5, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void SixButton_Click ( object sender, EventArgs e )
        {
            Handler.insertValue( 6, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void SevenButton_Click ( object sender, EventArgs e )
        {
            Handler.insertValue( 7, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void EigthButton_Click ( object sender, EventArgs e )
        {
            Handler.insertValue( 8, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void NineButton_Click ( object sender, EventArgs e )
        {
            Handler.insertValue( 9, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void ZeroButton_Click ( object sender, EventArgs e )
        {
            Handler.insertValue( 0, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void PointButton_Click ( object sender, EventArgs e )
        {
            Handler.handlePoint( ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void DivideButton_Click ( object sender, EventArgs e )
        {
            if ( Handler.Values.Count != 0 )
                Handler.onOperation( "/", SC, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void MultiplyButton_Click ( object sender, EventArgs e )
        {
            if ( Handler.Values.Count != 0 )
                Handler.onOperation( "*", SC, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void DifferenceButton_Click ( object sender, EventArgs e )
        {
            if ( Handler.Values.Count != 0 )
                Handler.onOperation( "-", SC, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void SumButton_Click ( object sender, EventArgs e )
        {
            if ( Handler.Values.Count != 0 )
                Handler.onOperation( "+", SC, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void handleOperation ()
        {
            switch ( Handler.Symbol )
            {
                case "+":
                    SC.evaluateSum();
                    break;
                case "-":
                    SC.evaluateDifference();
                    break;
                case "*":
                    SC.evaluateMultiplication();
                    break;
                case "/":
                    SC.evaluateDivision();
                    break;
            }
        }

/*---------------------------------------------------------------------------*/

        private void ResultButton_Click ( object sender, EventArgs e )
        {
            if ( Handler.Values.Count != 0 )
            {
                SC.SecondOperand 
                    = new MathValue( Convert.ToDouble( Handler.Values[ 1 ] ) );
                handleOperation();
                richTextBox.Text += "=" + SC.Result.Data.ToString() + "\n";
                richTextBox.SelectionStart = richTextBox.Text.Length;
                richTextBox.ScrollToCaret();
                clear();
            }
        }
        
/*---------------------------------------------------------------------------*/

        private bool isMatchingOperation(string _c)
        {
            return _c == "+" || _c == "-" || _c == "*" || _c == "/";
        }

/*---------------------------------------------------------------------------*/

        private void clear ()
        {
            Handler.clear();
            SC = new SimpleCalculator();
        }
    }
}


/*****************************************************************************/