﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using System;
using System.Linq;
using System.Windows.Forms;
using SimpleMathematika.Utilites.Values;
using SimpleMathematika.Model.Calculators;
/*****************************************************************************/


namespace SimpleMathematika.UserInterface.ProjectForms
{
    public partial class RomanCalcForm : Form
    {
        public RomanCalculator RC           { get; private set; }
        public BaseDigitCalcHandler Handler { get; private set; }
        public int FirstNumber              { get; private set; }
        public char Action                  { get; private set; }

/*---------------------------------------------------------------------------*/

        public RomanCalcForm()
        {
            RC      = new RomanCalculator();
            Handler = new BaseDigitCalcHandler();
            InitializeComponent();
        }

/*---------------------------------------------------------------------------*/

        private void romanCalcForm_Closing (
                object sender
            , FormClosingEventArgs e
        )
        {
            Application.Exit();
        }

/*---------------------------------------------------------------------------*/

        private void simpleCalculatorToolStripMenuItem_Click (
                object sender
            ,   EventArgs e
        )
        {
            var SCF = new SimpleCalcForm();
            SCF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void engineerCalculatorToolStripMenuItem_Click (
                object sender
            ,   EventArgs e
        )
        {
            var ECF = new EngineerCalcForm();
            ECF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void formulaToolStripMenuItem_Click ( 
                object sender
            ,   EventArgs e 
        )
        {
            var FF = new FormulaForm();
            FF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void graphicaCalculatorToolStripMenuItem_Click ( 
                object sender
            ,   EventArgs e 
        )
        {
            var GF = new GraphicaForm();
            GF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void converterToolStripMenuItem_Click ( 
                object sender
            ,   EventArgs e
        )
        {
            var C = new ConverterForm();
            C.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void authorToolStripMenuItem_Click(object sender, EventArgs e)
        {
             MessageBox.Show( 
                    "Invented by Yevheii Mamontov, NURE 2017." 
                +   "\nFor contact use yevhenii.mamontov@nure.ua" 
            );
        }

/*---------------------------------------------------------------------------*/

        private void ConvertButton_Click ( object sender, EventArgs e )
        {
            if ( RC.Data.Length != 0 )
            {
                RC.convertToArabianValue();
                richTextBox.Text = RC.Result.Data.ToString();
                RC.Data = "";
            }
            else
            {
                double valueToConvert = Convert.ToDouble( richTextBox.Text );
                RC.Result = new RomanValueResult( valueToConvert );
                RC.Data = RC.convertToRomanValue();
                richTextBox.Text = RC.Data;
            }

            richTextBox.Text += "\n";
        }

/*---------------------------------------------------------------------------*/

        private void ClearButton_Click ( object sender, EventArgs e )
        {
            RC.Result = new RomanValueResult( 0.0 );
            richTextBox.Text = "";
        }

/*---------------------------------------------------------------------------*/

        private void EraseButton_Click ( object sender, EventArgs e )
        {
            if ( RC.Data.Length != 0 || richTextBox.Text.Length != 0 )
            { 
                RC.Data = RC.Data.Remove( RC.Data.Length - 1 );
                richTextBox.Text 
                    = richTextBox.Text.Remove( richTextBox.Text.Length - 1 );
            }
        }

/*---------------------------------------------------------------------------*/

        private void OneButton_Click ( object sender, EventArgs e )
        {
            handleInsertDivByTenValue( "I" ); 
        }

/*---------------------------------------------------------------------------*/

        private void TwoButton_Click ( object sender, EventArgs e )
        {
            handleInsertDivByTenValue( "II", true /*multiVal*/ ); 
        }

/*---------------------------------------------------------------------------*/

        private void ThreeButton_Click ( object sender, EventArgs e )
        {
            handleInsertDivByTenValue( "III", true /*multiVal*/ ); 
        }

/*---------------------------------------------------------------------------*/

        private void FiveButton_Click ( object sender, EventArgs e )
        {
            handleInsertDivByFiveValue( "V" );
        }

/*---------------------------------------------------------------------------*/

        private void TenButton_Click ( object sender, EventArgs e )
        {
            handleInsertDivByTenValue( "X" ); 
        }

/*---------------------------------------------------------------------------*/

        private void FiftyButton_Click ( object sender, EventArgs e )
        {
            handleInsertDivByFiveValue( "L" );
        }

/*---------------------------------------------------------------------------*/

        private void OneHunderButton_Click ( object sender, EventArgs e )
        {
            handleInsertDivByTenValue( "X" ); 
        }

/*---------------------------------------------------------------------------*/

        private void FiveHunderButton_Click ( object sender, EventArgs e )
        {
            handleInsertDivByFiveValue( "D" );
        }

/*---------------------------------------------------------------------------*/

        private void ThousandButton_Click ( object sender, EventArgs e )
        {
            handleInsertDivByTenValue( "M" );
        }

/*---------------------------------------------------------------------------*/

        private void MultiplyButton_Click ( object sender, EventArgs e )
        {
            handleInsertOperation( '*' );
        }

/*---------------------------------------------------------------------------*/

        private void DifferenceButton_Click ( object sender, EventArgs e )
        {
            handleInsertOperation( '-' );
        }

/*---------------------------------------------------------------------------*/

        private void SumButton_Click ( object sender, EventArgs e )
        {
            handleInsertOperation( '+' );
        }

/*---------------------------------------------------------------------------*/

        private void ResultButton_Click ( object sender, EventArgs e )
        {
            RC.convertToArabianValue();
            int SecondNumber = RC.Result.toInt();

            if ( Action == '+' )
                RC.Result = new RomanValueResult( FirstNumber + SecondNumber );
            else if ( Action == '-' )
                RC.Result = new RomanValueResult( FirstNumber - SecondNumber );
            else if ( Action == '*' )
                RC.Result = new RomanValueResult( FirstNumber * SecondNumber );

            RC.Data = RC.convertToRomanValue();

            richTextBox.Text += "=" + RC.Data + "\n";
        }

/*---------------------------------------------------------------------------*/

        private void handleInsertDivByTenValue ( 
                string _value        
            ,   bool _isMultiVal = false 
        )
        {
            RC.Data += _value;
            richTextBox.AppendText( _value );

            if ( ! _isMultiVal )
                if ( !RC.isCorrectRepeatedDivisibleByTen() )
                {
                    MessageBox.Show( "Too few the same arguments." );
                    RC.Data = RC.Data.Remove( RC.Data.Length - 1 );
                    richTextBox.Text = RC.Data;
                }
            else
                if ( !RC.isCorrectRepeatedDivisibleByTen() )
                {
                    MessageBox.Show( "Too few the same arguments." );

                    int counter = RC.Data.Count( el => el == 'I' );
                    while ( counter != 3 )
                    {
                        RC.Data = RC.Data.Remove( RC.Data.Length - 1 );
                        --counter;
                    }

                    richTextBox.Text = RC.Data;
                }
        }

 /*---------------------------------------------------------------------------*/

        private void handleInsertDivByFiveValue ( string _value )
        {
            RC.Data += _value;
            richTextBox.AppendText( _value );

            if ( RC.isRepeatedDivisibleByFive() )
            {
                MessageBox.Show( "Too few the same arguments." );
                RC.Data = RC.Data.Remove( RC.Data.Length - 1 );
                richTextBox.Text = RC.Data;
            }
        }

/*---------------------------------------------------------------------------*/

        private void handleInsertOperation ( char _operKind )
        {
            RC.convertToArabianValue();
            FirstNumber = RC.Result.toInt();
            Action = _operKind;

            clear();
            richTextBox.Text += _operKind;
        }

/*---------------------------------------------------------------------------*/

        private void clear ()
        {
            RC.Data = "";
            RC.Result = new RomanValueResult( 0.0 );
        }
    }
}


/*****************************************************************************/