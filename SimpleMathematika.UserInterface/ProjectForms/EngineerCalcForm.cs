﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using System;
using System.Windows.Forms;
using SimpleMathematika.Utilites.Values;
using SimpleMathematika.Model.Calculators;
/*****************************************************************************/


namespace SimpleMathematika.UserInterface.ProjectForms
{
    public partial class EngineerCalcForm : Form
    {
        public EngineerCalculator EC { get; private set; }
        public BaseDigitCalcHandler Handler { get; private set; }

/*---------------------------------------------------------------------------*/

        public EngineerCalcForm ()
        {
            EC      = new EngineerCalculator();
            Handler = new BaseDigitCalcHandler();
            InitializeComponent();
        }

/*---------------------------------------------------------------------------*/

        private void EngineerCalcForm_Closing ( 
                object sender
            ,   FormClosingEventArgs e 
        )
        {
            Application.Exit();
        }


/*---------------------------------------------------------------------------*/

        private void simpleCalculatorToolStripMenuItem_Click ( 
                object sender
            ,   EventArgs e 
        )
        {
            var SCF = new SimpleCalcForm();
            SCF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void romanCalculatorToolStripMenuItem_Click (
                object sender
            ,   EventArgs e
        )
        {
            var RCF = new RomanCalcForm();
            RCF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void formulaToolStripMenuItem_Click ( 
                object sender
            ,   EventArgs e 
        )
        {
            var FF = new FormulaForm();
            FF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void graphicaToolStripMenuItem_Click ( 
                object sender
            ,   EventArgs e 
        )
        {
            var GF = new GraphicaForm();
            GF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void converterToolStripMenuItem_Click ( 
                object sender
            ,   EventArgs e
        )
        {
            var C = new ConverterForm();
            C.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void authorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show( 
                    "Invented by Yevheii Mamontov, NURE 2017." 
                +   "\nFor contact use yevhenii.mamontov@nure.ua" 
            );
        }

/*---------------------------------------------------------------------------*/

        private void CEButton_Click ( object sender, EventArgs e )
        {
            Handler.handleCE( EC, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void ClearButton_Click ( object sender, EventArgs e )
        {
            Handler.handleClear( EC, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void EraseButton_Click ( object sender, EventArgs e )
        {
            Handler.handleErase( ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void OneButton_Click ( object sender, EventArgs e )
        {
            Handler.insertValue( 1, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void TwoButton_Click ( object sender, EventArgs e )
        {
            Handler.insertValue( 2, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void ThreeButton_Click ( object sender, EventArgs e )
        {
            Handler.insertValue( 3, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void FourButton_Click ( object sender, EventArgs e )
        {
            Handler.insertValue( 4, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void FiveButton_Click ( object sender, EventArgs e )
        {
            Handler.insertValue( 5, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void SixButton_Click ( object sender, EventArgs e )
        {
            Handler.insertValue( 6, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void SevenButton_Click ( object sender, EventArgs e )
        {
            Handler.insertValue( 7, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void EigthButton_Click ( object sender, EventArgs e )
        {
            Handler.insertValue( 8, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void NineButton_Click ( object sender, EventArgs e )
        {
            Handler.insertValue( 9, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void ZeroButton_Click ( object sender, EventArgs e )
        {
            Handler.insertValue( 0, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void PointButton_Click ( object sender, EventArgs e )
        {
            Handler.handlePoint( ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void SinButton_Click ( object sender, EventArgs e )
        {
            if ( Handler.Values.Count != 0 )
            { 
                EC.Angle = new MathValue( Convert.ToDouble( Handler.Values[ 0 ] ) );
                EC.evaluateSin();

                appendToRTB( "sin" );
            }
        }

/*---------------------------------------------------------------------------*/

        private void CosButton_Click ( object sender, EventArgs e )
        {
            if ( Handler.Values.Count != 0 )
            { 
                EC.Angle = new MathValue( Convert.ToDouble( Handler.Values[ 0 ] ) );
                EC.evaluateSin();
                appendToRTB( "cos" );
            }
        }

/*---------------------------------------------------------------------------*/

        private void TanButton_Click ( object sender, EventArgs e )
        {
            if ( Handler.Values.Count != 0 )
            { 
                EC.Angle = new MathValue( Convert.ToDouble( Handler.Values[ 0 ] ) );
                EC.evaluateSin();
                appendToRTB( "tan" );
            }
        }

/*---------------------------------------------------------------------------*/

        private void LogButton_Click ( object sender, EventArgs e )
        {
            if ( Handler.Values.Count != 0 )
            { 
                EC.FirstOperand 
                    = new MathValue( Convert.ToDouble( Handler.Values[ 0 ] ) );
                EC.evaluateLog();
                appendToRTB( "log" );
            }
        }

/*---------------------------------------------------------------------------*/

        private void PowX2Button_Click ( object sender, EventArgs e )
        {
            if ( Handler.Values.Count != 0 )
            { 
                EC.FirstOperand 
                    = new MathValue( Convert.ToDouble( Handler.Values[ 0 ] ) );
                EC.evaluatePower2();
                appendToRTB( " ^2 ", false );
            }
        }

/*---------------------------------------------------------------------------*/

        private void PowerXNButton_Click ( object sender, EventArgs e )
        {
            Handler.onOperation( "pow", EC, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void SqrtButton_Click ( object sender, EventArgs e )
        {
            if ( Handler.Values.Count != 0 )
            { 
                EC.FirstOperand 
                    = new MathValue( Convert.ToDouble( Handler.Values[ 0 ] ) );
                EC.evaluatePow2();
                appendToRTB(" sqrt2 ", false);
            }
        }

/*---------------------------------------------------------------------------*/

        private void SqrtNButton_Click ( object sender, EventArgs e )
        {
            Handler.onOperation( "sqrt", EC, ref richTextBox );
        }

/*---------------------------------------------------------------------------*/

        private void handleOperation ()
        {
            switch ( Handler.Symbol )
            {
                case "pow":
                    EC.evaluatePower();
                    break;
                case "sqrt":
                    EC.evaluatePow();
                    break;
            }
        }

/*---------------------------------------------------------------------------*/

        private void ResultButton_Click ( object sender, EventArgs e )
        {
            if ( Handler.Values.Count != 0 )
            {
                EC.SecondOperand 
                    = new MathValue( Convert.ToDouble( Handler.Values[ 1 ] ) );
                handleOperation();
                richTextBox.Text += "=" + EC.Result.Data.ToString() + "\n";
                clear();
            }
        }

/*---------------------------------------------------------------------------*/

        private void appendToRTB ( string _value, bool _isTrigonometr = true )
        {
            richTextBox.Text 
                    = richTextBox.Text.Substring( 
                            0
                        ,   richTextBox.Text.Length - 1 
                    );

            if ( _isTrigonometr )
                richTextBox.Text +=
                        _value
                    +   "(" 
                    +   EC.Angle.Data.ToString() 
                    +   ") = " 
                    +   EC.Result.Data.ToString()
                    +   "\n"
                ;
            else
                richTextBox.Text +=
                        EC.FirstOperand.Data.ToString()
                    +   _value
                    +   " = "
                    +   EC.Result.Data.ToString()        
                    +   "\n"
                ;
                clear();
        }
/*---------------------------------------------------------------------------*/

        private void clear ()
        {
            Handler.clear();
            EC = new EngineerCalculator();
        }
    }
}


/*****************************************************************************/