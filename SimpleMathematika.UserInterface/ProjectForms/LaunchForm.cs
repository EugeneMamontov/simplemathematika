﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using System;
using System.Windows.Forms;
using SimpleMathematika.UserInterface.ProjectForms;
using System.ComponentModel;
/*****************************************************************************/


namespace SimpleMathematika.UserInterface
{
    public partial class Launcher : Form
    {
        public Launcher ()
        {
            InitializeComponent();
        }

/*---------------------------------------------------------------------------*/

        private void Launcher_Load ( object sender, EventArgs e )
        {
            timer.Enabled = true;
        }

/*---------------------------------------------------------------------------*/

        private void timer1_Tick ( object sender, EventArgs e )
        {
            progressBar.Increment( 10 );

            if ( progressBar.Value == 100 )
            { 
                timer.Enabled = false;
                var SimpleCalcWindow = new SimpleCalcForm();
                SimpleCalcWindow.Show();
                this.Hide();
            }
        }
        
/*---------------------------------------------------------------------------*/

        private void LauncherForm_Closing ( 
                object sender
            ,   FormClosingEventArgs e
        )
        {
            Application.Exit();
        }
    }
}


/*****************************************************************************/