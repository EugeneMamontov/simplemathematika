﻿namespace SimpleMathematika.UserInterface.ProjectForms
{
    partial class FormulaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.simpleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.engineerCalculatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.romanCalculatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formulaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.authorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.richTextBox = new System.Windows.Forms.RichTextBox();
            this.ClearButton = new System.Windows.Forms.Button();
            this.EvaluateButton = new System.Windows.Forms.Button();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.ClearButton2 = new System.Windows.Forms.Button();
            this.labelRTB = new System.Windows.Forms.Label();
            this.labelRTB2 = new System.Windows.Forms.Label();
            this.SetXButton = new System.Windows.Forms.Button();
            this.textBoxValue = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.labelValue = new System.Windows.Forms.Label();
            this.EvaluateSEButton = new System.Windows.Forms.Button();
            this.converterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(655, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.simpleToolStripMenuItem,
            this.engineerCalculatorToolStripMenuItem,
            this.romanCalculatorToolStripMenuItem,
            this.formulaToolStripMenuItem,
            this.converterToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // simpleToolStripMenuItem
            // 
            this.simpleToolStripMenuItem.Name = "simpleToolStripMenuItem";
            this.simpleToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.simpleToolStripMenuItem.Text = "Simple Calculator";
            this.simpleToolStripMenuItem.Click += new System.EventHandler(this.simpleCalculatorToolStripMenuItem_Click);
            // 
            // engineerCalculatorToolStripMenuItem
            // 
            this.engineerCalculatorToolStripMenuItem.Name = "engineerCalculatorToolStripMenuItem";
            this.engineerCalculatorToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.engineerCalculatorToolStripMenuItem.Text = "Engineer Calculator";
            this.engineerCalculatorToolStripMenuItem.Click += new System.EventHandler(this.engineerCalculatorToolStripMenuItem_Click);
            // 
            // romanCalculatorToolStripMenuItem
            // 
            this.romanCalculatorToolStripMenuItem.Name = "romanCalculatorToolStripMenuItem";
            this.romanCalculatorToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.romanCalculatorToolStripMenuItem.Text = "Roman Calculator";
            this.romanCalculatorToolStripMenuItem.Click += new System.EventHandler(this.romanCalculatorToolStripMenuItem_Click);
            // 
            // formulaToolStripMenuItem
            // 
            this.formulaToolStripMenuItem.Name = "formulaToolStripMenuItem";
            this.formulaToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.formulaToolStripMenuItem.Text = "Graphica";
            this.formulaToolStripMenuItem.Click += new System.EventHandler(this.graphicaToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.authorToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // authorToolStripMenuItem
            // 
            this.authorToolStripMenuItem.Name = "authorToolStripMenuItem";
            this.authorToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.authorToolStripMenuItem.Text = "Author";
            this.authorToolStripMenuItem.Click += new System.EventHandler(this.authorToolStripMenuItem_Click);
            // 
            // richTextBox
            // 
            this.richTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBox.Location = new System.Drawing.Point(12, 59);
            this.richTextBox.Name = "richTextBox";
            this.richTextBox.Size = new System.Drawing.Size(346, 180);
            this.richTextBox.TabIndex = 2;
            this.richTextBox.Text = "";
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(283, 255);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(75, 23);
            this.ClearButton.TabIndex = 42;
            this.ClearButton.Text = "Clear";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // EvaluateButton
            // 
            this.EvaluateButton.Location = new System.Drawing.Point(202, 255);
            this.EvaluateButton.Name = "EvaluateButton";
            this.EvaluateButton.Size = new System.Drawing.Size(75, 23);
            this.EvaluateButton.TabIndex = 43;
            this.EvaluateButton.Text = "Evaluate";
            this.EvaluateButton.UseVisualStyleBackColor = true;
            this.EvaluateButton.Click += new System.EventHandler(this.EvaluateButton_Click);
            // 
            // richTextBox2
            // 
            this.richTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBox2.Location = new System.Drawing.Point(364, 59);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(283, 258);
            this.richTextBox2.TabIndex = 44;
            this.richTextBox2.Text = "";
            // 
            // ClearButton2
            // 
            this.ClearButton2.Location = new System.Drawing.Point(572, 326);
            this.ClearButton2.Name = "ClearButton2";
            this.ClearButton2.Size = new System.Drawing.Size(75, 23);
            this.ClearButton2.TabIndex = 45;
            this.ClearButton2.Text = "Clear";
            this.ClearButton2.UseVisualStyleBackColor = true;
            this.ClearButton2.Click += new System.EventHandler(this.ClearButton2_Click);
            // 
            // labelRTB
            // 
            this.labelRTB.AutoSize = true;
            this.labelRTB.Location = new System.Drawing.Point(12, 37);
            this.labelRTB.Name = "labelRTB";
            this.labelRTB.Size = new System.Drawing.Size(95, 13);
            this.labelRTB.TabIndex = 46;
            this.labelRTB.Text = "Input formula there";
            // 
            // labelRTB2
            // 
            this.labelRTB2.AutoSize = true;
            this.labelRTB2.Location = new System.Drawing.Point(361, 37);
            this.labelRTB2.Name = "labelRTB2";
            this.labelRTB2.Size = new System.Drawing.Size(89, 13);
            this.labelRTB2.TabIndex = 47;
            this.labelRTB2.Text = "Check out results";
            // 
            // SetXButton
            // 
            this.SetXButton.Location = new System.Drawing.Point(32, 326);
            this.SetXButton.Name = "SetXButton";
            this.SetXButton.Size = new System.Drawing.Size(75, 23);
            this.SetXButton.TabIndex = 48;
            this.SetXButton.Text = "Set";
            this.SetXButton.UseVisualStyleBackColor = true;
            this.SetXButton.Click += new System.EventHandler(this.SetXButton_Click);
            // 
            // textBoxValue
            // 
            this.textBoxValue.Location = new System.Drawing.Point(19, 297);
            this.textBoxValue.Name = "textBoxValue";
            this.textBoxValue.Size = new System.Drawing.Size(88, 20);
            this.textBoxValue.TabIndex = 49;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(19, 258);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(88, 20);
            this.textBoxName.TabIndex = 50;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(19, 242);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(35, 13);
            this.labelName.TabIndex = 51;
            this.labelName.Text = "Name";
            // 
            // labelValue
            // 
            this.labelValue.AutoSize = true;
            this.labelValue.Location = new System.Drawing.Point(19, 281);
            this.labelValue.Name = "labelValue";
            this.labelValue.Size = new System.Drawing.Size(34, 13);
            this.labelValue.TabIndex = 52;
            this.labelValue.Text = "Value";
            // 
            // EvaluateSEButton
            // 
            this.EvaluateSEButton.Location = new System.Drawing.Point(121, 255);
            this.EvaluateSEButton.Name = "EvaluateSEButton";
            this.EvaluateSEButton.Size = new System.Drawing.Size(75, 23);
            this.EvaluateSEButton.TabIndex = 53;
            this.EvaluateSEButton.Text = "SE";
            this.EvaluateSEButton.UseVisualStyleBackColor = true;
            this.EvaluateSEButton.Click += new System.EventHandler(this.EvaluateSEButton_Click);
            // 
            // converterToolStripMenuItem
            // 
            this.converterToolStripMenuItem.Name = "converterToolStripMenuItem";
            this.converterToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.converterToolStripMenuItem.Text = "Converter";
            this.converterToolStripMenuItem.Click += new System.EventHandler(this.converterToolStripMenuItem_Click);
            // 
            // FormulaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(655, 361);
            this.Controls.Add(this.EvaluateSEButton);
            this.Controls.Add(this.labelValue);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.textBoxValue);
            this.Controls.Add(this.SetXButton);
            this.Controls.Add(this.labelRTB2);
            this.Controls.Add(this.labelRTB);
            this.Controls.Add(this.ClearButton2);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.EvaluateButton);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.richTextBox);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormulaForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormulaForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formulaForm_Closing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem simpleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem engineerCalculatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem romanCalculatorToolStripMenuItem;
        private System.Windows.Forms.RichTextBox richTextBox;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.Button EvaluateButton;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Button ClearButton2;
        private System.Windows.Forms.Label labelRTB;
        private System.Windows.Forms.Label labelRTB2;
        private System.Windows.Forms.Button SetXButton;
        private System.Windows.Forms.TextBox textBoxValue;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelValue;
        private System.Windows.Forms.Button EvaluateSEButton;
        private System.Windows.Forms.ToolStripMenuItem formulaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem authorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem converterToolStripMenuItem;
    }
}