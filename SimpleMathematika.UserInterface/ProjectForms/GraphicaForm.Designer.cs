﻿namespace SimpleMathematika.UserInterface.ProjectForms
{
    partial class GraphicaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.simpleCalculatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.engineerCalculatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.romanCalculatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formulaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.authorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.SinButton = new System.Windows.Forms.Button();
            this.CosButton = new System.Windows.Forms.Button();
            this.TanButton = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            this.SinHButton = new System.Windows.Forms.Button();
            this.CosHButton = new System.Windows.Forms.Button();
            this.TanHButton = new System.Windows.Forms.Button();
            this.LinearGraphButton = new System.Windows.Forms.Button();
            this.SquardGraphButton = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.converterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(630, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.simpleCalculatorToolStripMenuItem,
            this.engineerCalculatorToolStripMenuItem,
            this.romanCalculatorToolStripMenuItem,
            this.formulaToolStripMenuItem,
            this.converterToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // simpleCalculatorToolStripMenuItem
            // 
            this.simpleCalculatorToolStripMenuItem.Name = "simpleCalculatorToolStripMenuItem";
            this.simpleCalculatorToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.simpleCalculatorToolStripMenuItem.Text = "Simple Calculator";
            this.simpleCalculatorToolStripMenuItem.Click += new System.EventHandler(this.simpleCalculatorToolStripMenuItem_Click);
            // 
            // engineerCalculatorToolStripMenuItem
            // 
            this.engineerCalculatorToolStripMenuItem.Name = "engineerCalculatorToolStripMenuItem";
            this.engineerCalculatorToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.engineerCalculatorToolStripMenuItem.Text = "Engineer Calculator";
            this.engineerCalculatorToolStripMenuItem.Click += new System.EventHandler(this.engineerCalculatorToolStripMenuItem_Click);
            // 
            // romanCalculatorToolStripMenuItem
            // 
            this.romanCalculatorToolStripMenuItem.Name = "romanCalculatorToolStripMenuItem";
            this.romanCalculatorToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.romanCalculatorToolStripMenuItem.Text = "Roman Calculator";
            this.romanCalculatorToolStripMenuItem.Click += new System.EventHandler(this.romanCalculatorToolStripMenuItem_Click);
            // 
            // formulaToolStripMenuItem
            // 
            this.formulaToolStripMenuItem.Name = "formulaToolStripMenuItem";
            this.formulaToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.formulaToolStripMenuItem.Text = "Formula";
            this.formulaToolStripMenuItem.Click += new System.EventHandler(this.formulaToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.authorToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // authorToolStripMenuItem
            // 
            this.authorToolStripMenuItem.Name = "authorToolStripMenuItem";
            this.authorToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.authorToolStripMenuItem.Text = "Author";
            this.authorToolStripMenuItem.Click += new System.EventHandler(this.authorToolStripMenuItem_Click);
            // 
            // chart
            // 
            chartArea1.Name = "ChartArea1";
            this.chart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart.Legends.Add(legend1);
            this.chart.Location = new System.Drawing.Point(13, 28);
            this.chart.Name = "chart";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Legend = "Legend1";
            series1.LegendText = "sin";
            series1.Name = "Series1";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series2.Legend = "Legend1";
            series2.LegendText = "cos";
            series2.Name = "Series2";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series3.Legend = "Legend1";
            series3.LegendText = "tan";
            series3.Name = "Series3";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series4.Legend = "Legend1";
            series4.LegendText = "sinh";
            series4.Name = "Series4";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series5.Legend = "Legend1";
            series5.LegendText = "cosh";
            series5.Name = "Series5";
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series6.Legend = "Legend1";
            series6.LegendText = "tanh";
            series6.Name = "Series6";
            series7.ChartArea = "ChartArea1";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series7.Legend = "Legend1";
            series7.LegendText = "y = k( x )";
            series7.Name = "Series7";
            series7.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Chocolate;
            series8.ChartArea = "ChartArea1";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series8.Legend = "Legend1";
            series8.LegendText = "y = ( x )^2";
            series8.Name = "Series8";
            this.chart.Series.Add(series1);
            this.chart.Series.Add(series2);
            this.chart.Series.Add(series3);
            this.chart.Series.Add(series4);
            this.chart.Series.Add(series5);
            this.chart.Series.Add(series6);
            this.chart.Series.Add(series7);
            this.chart.Series.Add(series8);
            this.chart.Size = new System.Drawing.Size(605, 204);
            this.chart.TabIndex = 1;
            this.chart.Text = "chart1";
            title1.Name = "Title1";
            title1.Text = "Function graphic";
            this.chart.Titles.Add(title1);
            // 
            // SinButton
            // 
            this.SinButton.Location = new System.Drawing.Point(543, 238);
            this.SinButton.Name = "SinButton";
            this.SinButton.Size = new System.Drawing.Size(75, 23);
            this.SinButton.TabIndex = 2;
            this.SinButton.Text = "sin";
            this.SinButton.UseVisualStyleBackColor = true;
            this.SinButton.Click += new System.EventHandler(this.SinButton_Click);
            // 
            // CosButton
            // 
            this.CosButton.Location = new System.Drawing.Point(462, 238);
            this.CosButton.Name = "CosButton";
            this.CosButton.Size = new System.Drawing.Size(75, 23);
            this.CosButton.TabIndex = 3;
            this.CosButton.Text = "cos";
            this.CosButton.UseVisualStyleBackColor = true;
            this.CosButton.Click += new System.EventHandler(this.CosButton_Click);
            // 
            // TanButton
            // 
            this.TanButton.Location = new System.Drawing.Point(381, 238);
            this.TanButton.Name = "TanButton";
            this.TanButton.Size = new System.Drawing.Size(75, 23);
            this.TanButton.TabIndex = 4;
            this.TanButton.Text = "tan";
            this.TanButton.UseVisualStyleBackColor = true;
            this.TanButton.Click += new System.EventHandler(this.TanButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(219, 239);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(75, 23);
            this.ClearButton.TabIndex = 5;
            this.ClearButton.Text = "clear";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // SinHButton
            // 
            this.SinHButton.Location = new System.Drawing.Point(543, 267);
            this.SinHButton.Name = "SinHButton";
            this.SinHButton.Size = new System.Drawing.Size(75, 23);
            this.SinHButton.TabIndex = 6;
            this.SinHButton.Text = "sinH";
            this.SinHButton.UseVisualStyleBackColor = true;
            this.SinHButton.Click += new System.EventHandler(this.SinHButton_Click);
            // 
            // CosHButton
            // 
            this.CosHButton.Location = new System.Drawing.Point(462, 268);
            this.CosHButton.Name = "CosHButton";
            this.CosHButton.Size = new System.Drawing.Size(75, 23);
            this.CosHButton.TabIndex = 7;
            this.CosHButton.Text = "cosH";
            this.CosHButton.UseVisualStyleBackColor = true;
            this.CosHButton.Click += new System.EventHandler(this.CosHButton_Click);
            // 
            // TanHButton
            // 
            this.TanHButton.Location = new System.Drawing.Point(381, 267);
            this.TanHButton.Name = "TanHButton";
            this.TanHButton.Size = new System.Drawing.Size(75, 23);
            this.TanHButton.TabIndex = 8;
            this.TanHButton.Text = "tanH";
            this.TanHButton.UseVisualStyleBackColor = true;
            this.TanHButton.Click += new System.EventHandler(this.TanHButton_Click);
            // 
            // LinearGraphButton
            // 
            this.LinearGraphButton.Location = new System.Drawing.Point(300, 239);
            this.LinearGraphButton.Name = "LinearGraphButton";
            this.LinearGraphButton.Size = new System.Drawing.Size(75, 23);
            this.LinearGraphButton.TabIndex = 9;
            this.LinearGraphButton.Text = "y = k( x )";
            this.LinearGraphButton.UseVisualStyleBackColor = true;
            this.LinearGraphButton.Click += new System.EventHandler(this.LinearGraphButton_Click);
            // 
            // SquardGraphButton
            // 
            this.SquardGraphButton.Location = new System.Drawing.Point(300, 267);
            this.SquardGraphButton.Name = "SquardGraphButton";
            this.SquardGraphButton.Size = new System.Drawing.Size(75, 23);
            this.SquardGraphButton.TabIndex = 10;
            this.SquardGraphButton.Text = "y = ( x )^2";
            this.SquardGraphButton.UseVisualStyleBackColor = true;
            this.SquardGraphButton.Click += new System.EventHandler(this.SquardGraphButton_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(16, 254);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 238);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Input K";
            // 
            // converterToolStripMenuItem
            // 
            this.converterToolStripMenuItem.Name = "converterToolStripMenuItem";
            this.converterToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.converterToolStripMenuItem.Text = "Converter";
            this.converterToolStripMenuItem.Click += new System.EventHandler(this.converterToolStripMenuItem_Click);
            // 
            // GraphicaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 303);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.SquardGraphButton);
            this.Controls.Add(this.LinearGraphButton);
            this.Controls.Add(this.TanHButton);
            this.Controls.Add(this.CosHButton);
            this.Controls.Add(this.SinHButton);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.TanButton);
            this.Controls.Add(this.CosButton);
            this.Controls.Add(this.SinButton);
            this.Controls.Add(this.chart);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "GraphicaForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GraphicaForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.graphicaForm_Closing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem simpleCalculatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem engineerCalculatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem romanCalculatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formulaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart;
        private System.Windows.Forms.Button SinButton;
        private System.Windows.Forms.Button CosButton;
        private System.Windows.Forms.Button TanButton;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.Button SinHButton;
        private System.Windows.Forms.Button CosHButton;
        private System.Windows.Forms.Button TanHButton;
        private System.Windows.Forms.Button LinearGraphButton;
        private System.Windows.Forms.Button SquardGraphButton;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem authorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem converterToolStripMenuItem;
    }
}