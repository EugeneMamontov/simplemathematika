﻿namespace SimpleMathematika.UserInterface.ProjectForms
{
    partial class EngineerCalcForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox = new System.Windows.Forms.RichTextBox();
            this.PointButton = new System.Windows.Forms.Button();
            this.ZeroButton = new System.Windows.Forms.Button();
            this.NineButton = new System.Windows.Forms.Button();
            this.EigthButton = new System.Windows.Forms.Button();
            this.SevenButton = new System.Windows.Forms.Button();
            this.SixButton = new System.Windows.Forms.Button();
            this.FiveButton = new System.Windows.Forms.Button();
            this.FourButton = new System.Windows.Forms.Button();
            this.ThreeButton = new System.Windows.Forms.Button();
            this.TwoButton = new System.Windows.Forms.Button();
            this.OneButton = new System.Windows.Forms.Button();
            this.CEButton = new System.Windows.Forms.Button();
            this.EraseButton = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            this.SinButton = new System.Windows.Forms.Button();
            this.CosButton = new System.Windows.Forms.Button();
            this.TanButton = new System.Windows.Forms.Button();
            this.LogButton = new System.Windows.Forms.Button();
            this.PowX2Button = new System.Windows.Forms.Button();
            this.PowerXNButton = new System.Windows.Forms.Button();
            this.SqrtButton = new System.Windows.Forms.Button();
            this.ResultButton = new System.Windows.Forms.Button();
            this.SqrtNButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.engineerCalculatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.romanCalculatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formulaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.graphicaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.converterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.authorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBox
            // 
            this.richTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBox.Location = new System.Drawing.Point(12, 29);
            this.richTextBox.Name = "richTextBox";
            this.richTextBox.ReadOnly = true;
            this.richTextBox.Size = new System.Drawing.Size(396, 68);
            this.richTextBox.TabIndex = 1;
            this.richTextBox.Text = "";
            // 
            // PointButton
            // 
            this.PointButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PointButton.Location = new System.Drawing.Point(172, 221);
            this.PointButton.Name = "PointButton";
            this.PointButton.Size = new System.Drawing.Size(75, 24);
            this.PointButton.TabIndex = 28;
            this.PointButton.Text = ",";
            this.PointButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.PointButton.UseVisualStyleBackColor = true;
            this.PointButton.Click += new System.EventHandler(this.PointButton_Click);
            // 
            // ZeroButton
            // 
            this.ZeroButton.Location = new System.Drawing.Point(91, 221);
            this.ZeroButton.Name = "ZeroButton";
            this.ZeroButton.Size = new System.Drawing.Size(75, 23);
            this.ZeroButton.TabIndex = 27;
            this.ZeroButton.Text = "0";
            this.ZeroButton.UseVisualStyleBackColor = true;
            this.ZeroButton.Click += new System.EventHandler(this.ZeroButton_Click);
            // 
            // NineButton
            // 
            this.NineButton.Location = new System.Drawing.Point(172, 192);
            this.NineButton.Name = "NineButton";
            this.NineButton.Size = new System.Drawing.Size(75, 23);
            this.NineButton.TabIndex = 26;
            this.NineButton.Text = "9";
            this.NineButton.UseVisualStyleBackColor = true;
            this.NineButton.Click += new System.EventHandler(this.NineButton_Click);
            // 
            // EigthButton
            // 
            this.EigthButton.Location = new System.Drawing.Point(91, 191);
            this.EigthButton.Name = "EigthButton";
            this.EigthButton.Size = new System.Drawing.Size(75, 23);
            this.EigthButton.TabIndex = 25;
            this.EigthButton.Text = "8";
            this.EigthButton.UseVisualStyleBackColor = true;
            this.EigthButton.Click += new System.EventHandler(this.EigthButton_Click);
            // 
            // SevenButton
            // 
            this.SevenButton.Location = new System.Drawing.Point(12, 190);
            this.SevenButton.Name = "SevenButton";
            this.SevenButton.Size = new System.Drawing.Size(75, 23);
            this.SevenButton.TabIndex = 24;
            this.SevenButton.Text = "7";
            this.SevenButton.UseVisualStyleBackColor = true;
            this.SevenButton.Click += new System.EventHandler(this.SevenButton_Click);
            // 
            // SixButton
            // 
            this.SixButton.Location = new System.Drawing.Point(172, 161);
            this.SixButton.Name = "SixButton";
            this.SixButton.Size = new System.Drawing.Size(75, 23);
            this.SixButton.TabIndex = 23;
            this.SixButton.Text = "6";
            this.SixButton.UseVisualStyleBackColor = true;
            this.SixButton.Click += new System.EventHandler(this.SixButton_Click);
            // 
            // FiveButton
            // 
            this.FiveButton.Location = new System.Drawing.Point(91, 162);
            this.FiveButton.Name = "FiveButton";
            this.FiveButton.Size = new System.Drawing.Size(75, 23);
            this.FiveButton.TabIndex = 22;
            this.FiveButton.Text = "5";
            this.FiveButton.UseVisualStyleBackColor = true;
            this.FiveButton.Click += new System.EventHandler(this.FiveButton_Click);
            // 
            // FourButton
            // 
            this.FourButton.Location = new System.Drawing.Point(12, 161);
            this.FourButton.Name = "FourButton";
            this.FourButton.Size = new System.Drawing.Size(75, 23);
            this.FourButton.TabIndex = 21;
            this.FourButton.Text = "4";
            this.FourButton.UseVisualStyleBackColor = true;
            this.FourButton.Click += new System.EventHandler(this.FourButton_Click);
            // 
            // ThreeButton
            // 
            this.ThreeButton.Location = new System.Drawing.Point(172, 133);
            this.ThreeButton.Name = "ThreeButton";
            this.ThreeButton.Size = new System.Drawing.Size(75, 23);
            this.ThreeButton.TabIndex = 20;
            this.ThreeButton.Text = "3";
            this.ThreeButton.UseVisualStyleBackColor = true;
            this.ThreeButton.Click += new System.EventHandler(this.ThreeButton_Click);
            // 
            // TwoButton
            // 
            this.TwoButton.Location = new System.Drawing.Point(91, 133);
            this.TwoButton.Name = "TwoButton";
            this.TwoButton.Size = new System.Drawing.Size(75, 23);
            this.TwoButton.TabIndex = 19;
            this.TwoButton.Text = "2";
            this.TwoButton.UseVisualStyleBackColor = true;
            this.TwoButton.Click += new System.EventHandler(this.TwoButton_Click);
            // 
            // OneButton
            // 
            this.OneButton.Location = new System.Drawing.Point(12, 132);
            this.OneButton.Name = "OneButton";
            this.OneButton.Size = new System.Drawing.Size(75, 23);
            this.OneButton.TabIndex = 18;
            this.OneButton.Text = "1";
            this.OneButton.UseVisualStyleBackColor = true;
            this.OneButton.Click += new System.EventHandler(this.OneButton_Click);
            // 
            // CEButton
            // 
            this.CEButton.Location = new System.Drawing.Point(12, 103);
            this.CEButton.Name = "CEButton";
            this.CEButton.Size = new System.Drawing.Size(75, 23);
            this.CEButton.TabIndex = 31;
            this.CEButton.Text = "CE";
            this.CEButton.UseVisualStyleBackColor = true;
            this.CEButton.Click += new System.EventHandler(this.CEButton_Click);
            // 
            // EraseButton
            // 
            this.EraseButton.Location = new System.Drawing.Point(172, 103);
            this.EraseButton.Name = "EraseButton";
            this.EraseButton.Size = new System.Drawing.Size(75, 23);
            this.EraseButton.TabIndex = 30;
            this.EraseButton.Text = "<-";
            this.EraseButton.UseVisualStyleBackColor = true;
            this.EraseButton.Click += new System.EventHandler(this.EraseButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(91, 103);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(75, 23);
            this.ClearButton.TabIndex = 29;
            this.ClearButton.Text = "C";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // SinButton
            // 
            this.SinButton.Location = new System.Drawing.Point(252, 103);
            this.SinButton.Name = "SinButton";
            this.SinButton.Size = new System.Drawing.Size(75, 23);
            this.SinButton.TabIndex = 32;
            this.SinButton.Text = "sin";
            this.SinButton.UseVisualStyleBackColor = true;
            this.SinButton.Click += new System.EventHandler(this.SinButton_Click);
            // 
            // CosButton
            // 
            this.CosButton.Location = new System.Drawing.Point(252, 133);
            this.CosButton.Name = "CosButton";
            this.CosButton.Size = new System.Drawing.Size(75, 23);
            this.CosButton.TabIndex = 33;
            this.CosButton.Text = "cos";
            this.CosButton.UseVisualStyleBackColor = true;
            this.CosButton.Click += new System.EventHandler(this.CosButton_Click);
            // 
            // TanButton
            // 
            this.TanButton.Location = new System.Drawing.Point(252, 162);
            this.TanButton.Name = "TanButton";
            this.TanButton.Size = new System.Drawing.Size(75, 23);
            this.TanButton.TabIndex = 34;
            this.TanButton.Text = "tan";
            this.TanButton.UseVisualStyleBackColor = true;
            this.TanButton.Click += new System.EventHandler(this.TanButton_Click);
            // 
            // LogButton
            // 
            this.LogButton.Location = new System.Drawing.Point(252, 192);
            this.LogButton.Name = "LogButton";
            this.LogButton.Size = new System.Drawing.Size(75, 23);
            this.LogButton.TabIndex = 35;
            this.LogButton.Text = "log";
            this.LogButton.UseVisualStyleBackColor = true;
            this.LogButton.Click += new System.EventHandler(this.LogButton_Click);
            // 
            // PowX2Button
            // 
            this.PowX2Button.Location = new System.Drawing.Point(333, 104);
            this.PowX2Button.Name = "PowX2Button";
            this.PowX2Button.Size = new System.Drawing.Size(75, 23);
            this.PowX2Button.TabIndex = 36;
            this.PowX2Button.Text = "x^2";
            this.PowX2Button.UseVisualStyleBackColor = true;
            this.PowX2Button.Click += new System.EventHandler(this.PowX2Button_Click);
            // 
            // PowerXNButton
            // 
            this.PowerXNButton.Location = new System.Drawing.Point(333, 133);
            this.PowerXNButton.Name = "PowerXNButton";
            this.PowerXNButton.Size = new System.Drawing.Size(75, 23);
            this.PowerXNButton.TabIndex = 37;
            this.PowerXNButton.Text = "x^n";
            this.PowerXNButton.UseVisualStyleBackColor = true;
            this.PowerXNButton.Click += new System.EventHandler(this.PowerXNButton_Click);
            // 
            // SqrtButton
            // 
            this.SqrtButton.Location = new System.Drawing.Point(333, 162);
            this.SqrtButton.Name = "SqrtButton";
            this.SqrtButton.Size = new System.Drawing.Size(75, 23);
            this.SqrtButton.TabIndex = 38;
            this.SqrtButton.Text = "sqrt";
            this.SqrtButton.UseVisualStyleBackColor = true;
            this.SqrtButton.Click += new System.EventHandler(this.SqrtButton_Click);
            // 
            // ResultButton
            // 
            this.ResultButton.Location = new System.Drawing.Point(252, 222);
            this.ResultButton.Name = "ResultButton";
            this.ResultButton.Size = new System.Drawing.Size(75, 23);
            this.ResultButton.TabIndex = 39;
            this.ResultButton.Text = "=";
            this.ResultButton.UseVisualStyleBackColor = true;
            this.ResultButton.Click += new System.EventHandler(this.ResultButton_Click);
            // 
            // SqrtNButton
            // 
            this.SqrtNButton.Location = new System.Drawing.Point(333, 191);
            this.SqrtNButton.Name = "SqrtNButton";
            this.SqrtNButton.Size = new System.Drawing.Size(75, 23);
            this.SqrtNButton.TabIndex = 40;
            this.SqrtNButton.Text = "sqrt(n)";
            this.SqrtNButton.UseVisualStyleBackColor = true;
            this.SqrtNButton.Click += new System.EventHandler(this.SqrtNButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(418, 24);
            this.menuStrip1.TabIndex = 42;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.engineerCalculatorToolStripMenuItem,
            this.romanCalculatorToolStripMenuItem,
            this.formulaToolStripMenuItem,
            this.graphicaToolStripMenuItem,
            this.converterToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // engineerCalculatorToolStripMenuItem
            // 
            this.engineerCalculatorToolStripMenuItem.Name = "engineerCalculatorToolStripMenuItem";
            this.engineerCalculatorToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.engineerCalculatorToolStripMenuItem.Text = "Simple Calculator";
            this.engineerCalculatorToolStripMenuItem.Click += new System.EventHandler(this.simpleCalculatorToolStripMenuItem_Click);
            // 
            // romanCalculatorToolStripMenuItem
            // 
            this.romanCalculatorToolStripMenuItem.Name = "romanCalculatorToolStripMenuItem";
            this.romanCalculatorToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.romanCalculatorToolStripMenuItem.Text = "Roman Calculator";
            this.romanCalculatorToolStripMenuItem.Click += new System.EventHandler(this.romanCalculatorToolStripMenuItem_Click);
            // 
            // formulaToolStripMenuItem
            // 
            this.formulaToolStripMenuItem.Name = "formulaToolStripMenuItem";
            this.formulaToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.formulaToolStripMenuItem.Text = "Formula";
            this.formulaToolStripMenuItem.Click += new System.EventHandler(this.formulaToolStripMenuItem_Click);
            // 
            // graphicaToolStripMenuItem
            // 
            this.graphicaToolStripMenuItem.Name = "graphicaToolStripMenuItem";
            this.graphicaToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.graphicaToolStripMenuItem.Text = "Graphica";
            this.graphicaToolStripMenuItem.Click += new System.EventHandler(this.graphicaToolStripMenuItem_Click);
            // 
            // converterToolStripMenuItem
            // 
            this.converterToolStripMenuItem.Name = "converterToolStripMenuItem";
            this.converterToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.converterToolStripMenuItem.Text = "Converter";
            this.converterToolStripMenuItem.Click += new System.EventHandler(this.converterToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.authorToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // authorToolStripMenuItem
            // 
            this.authorToolStripMenuItem.Name = "authorToolStripMenuItem";
            this.authorToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.authorToolStripMenuItem.Text = "Author";
            this.authorToolStripMenuItem.Click += new System.EventHandler(this.authorToolStripMenuItem_Click);
            // 
            // EngineerCalcForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(418, 249);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.SqrtNButton);
            this.Controls.Add(this.ResultButton);
            this.Controls.Add(this.SqrtButton);
            this.Controls.Add(this.PowerXNButton);
            this.Controls.Add(this.PowX2Button);
            this.Controls.Add(this.LogButton);
            this.Controls.Add(this.TanButton);
            this.Controls.Add(this.CosButton);
            this.Controls.Add(this.SinButton);
            this.Controls.Add(this.CEButton);
            this.Controls.Add(this.EraseButton);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.PointButton);
            this.Controls.Add(this.ZeroButton);
            this.Controls.Add(this.NineButton);
            this.Controls.Add(this.EigthButton);
            this.Controls.Add(this.SevenButton);
            this.Controls.Add(this.SixButton);
            this.Controls.Add(this.FiveButton);
            this.Controls.Add(this.FourButton);
            this.Controls.Add(this.ThreeButton);
            this.Controls.Add(this.TwoButton);
            this.Controls.Add(this.OneButton);
            this.Controls.Add(this.richTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "EngineerCalcForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EngineerCalcForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EngineerCalcForm_Closing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox;
        private System.Windows.Forms.Button PointButton;
        private System.Windows.Forms.Button ZeroButton;
        private System.Windows.Forms.Button NineButton;
        private System.Windows.Forms.Button EigthButton;
        private System.Windows.Forms.Button SevenButton;
        private System.Windows.Forms.Button SixButton;
        private System.Windows.Forms.Button FiveButton;
        private System.Windows.Forms.Button FourButton;
        private System.Windows.Forms.Button ThreeButton;
        private System.Windows.Forms.Button TwoButton;
        private System.Windows.Forms.Button OneButton;
        private System.Windows.Forms.Button CEButton;
        private System.Windows.Forms.Button EraseButton;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.Button SinButton;
        private System.Windows.Forms.Button CosButton;
        private System.Windows.Forms.Button TanButton;
        private System.Windows.Forms.Button LogButton;
        private System.Windows.Forms.Button PowX2Button;
        private System.Windows.Forms.Button PowerXNButton;
        private System.Windows.Forms.Button SqrtButton;
        private System.Windows.Forms.Button ResultButton;
        private System.Windows.Forms.Button SqrtNButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem engineerCalculatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem romanCalculatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formulaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem graphicaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem authorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem converterToolStripMenuItem;
    }
}