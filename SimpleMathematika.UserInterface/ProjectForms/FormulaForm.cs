﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using System;
using System.Windows.Forms;
using System.Collections.Generic;
using SimpleMathematika.Model;
using SimpleMathematika.Core.FormulaParser;
/*****************************************************************************/

namespace SimpleMathematika.UserInterface.ProjectForms
{
    public partial class FormulaForm : Form
    {
        public Formula F                { get; private set; }
        public List< string > Buffer    { get; set; }

/*---------------------------------------------------------------------------*/

        public FormulaForm ()
        {
            F = new Formula();
            InitializeComponent();
        }

/*---------------------------------------------------------------------------*/

        public void formulaForm_Closing (
                object sender
            ,   FormClosingEventArgs e
        )
        {
            Application.Exit();
        }

/*---------------------------------------------------------------------------*/

        private void simpleCalculatorToolStripMenuItem_Click (
                object sender
            ,   EventArgs e
        )
        {
            var SCF = new SimpleCalcForm();
            SCF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void engineerCalculatorToolStripMenuItem_Click (
                object sender
            ,   EventArgs e
        )
        {
            var ECF = new EngineerCalcForm();
            ECF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void romanCalculatorToolStripMenuItem_Click (
                object sender
            ,   EventArgs e
        )
        {
            var RCF = new RomanCalcForm();
            RCF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void graphicaToolStripMenuItem_Click ( 
                object sender
            ,   EventArgs e
        )
        {
            var GF = new GraphicaForm();
            GF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void converterToolStripMenuItem_Click ( 
                object sender
            ,   EventArgs e
        )
        {
            var C = new ConverterForm();
            C.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void authorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show( 
                    "Invented by Yevheii Mamontov, NURE 2017." 
                +   "\nFor contact use yevhenii.mamontov@nure.ua" 
            );
        }

/*---------------------------------------------------------------------------*/

        private void EvaluateButton_Click ( object sender, EventArgs e )
        {
            if ( richTextBox.Text.Length != 0 )
            {
                F.StringToParse = richTextBox.Text;
                F.tryParse();

                richTextBox2.AppendText( 
                    String.Format(
                            "evaluated ["
                        +   richTextBox.Text
                        +   " ] to "
                        +   F.Result.Data.ToString()
                        +   "\n"
                    ) 
                );

                clear();
            }
        }

/*---------------------------------------------------------------------------*/

        private void EvaluateSEButton_Click ( object sender, EventArgs e )
        {
            F.StringToParse = richTextBox.Text;
            EquatationRoots ER = F.evaluateSquardEquatation();

            richTextBox2.AppendText( 
                    String.Format(
                            "evaluated ["
                        +   richTextBox.Text
                        +   " ]. Root(s) : "
                        +   ER.ToString()
                        +   "\n"
                    ) 
                );
        }

/*---------------------------------------------------------------------------*/

        private void ClearButton_Click ( object sender, EventArgs e )
        {
            richTextBox.Clear();
        }

/*---------------------------------------------------------------------------*/

        private void ClearButton2_Click ( object sender, EventArgs e )
        {
            richTextBox2.Clear();
        }

/*---------------------------------------------------------------------------*/

        private void SetXButton_Click ( object sender, EventArgs e )
        {
            F.addExpressionMember( 
                    textBoxName.Text
                ,   Convert.ToDouble( textBoxValue.Text ) 
            );
        }

/*---------------------------------------------------------------------------*/

        private void clear ()
        {
            F.StringToParse = "";
            richTextBox.Text = "";
            F.Result = new Utilites.Values.MathValue( 0.0 );
        }
    }
}


/*****************************************************************************/