﻿namespace SimpleMathematika.UserInterface.ProjectForms
{
    partial class RomanCalcForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox = new System.Windows.Forms.RichTextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.simpleCalculatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.engineerCalculatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formulaFormToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.romanCalculatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.converterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.authorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ThousandButton = new System.Windows.Forms.Button();
            this.FiveHunderButton = new System.Windows.Forms.Button();
            this.OneHunderButton = new System.Windows.Forms.Button();
            this.FiftyButton = new System.Windows.Forms.Button();
            this.TenButton = new System.Windows.Forms.Button();
            this.FiveButton = new System.Windows.Forms.Button();
            this.ThreeButton = new System.Windows.Forms.Button();
            this.TwoButton = new System.Windows.Forms.Button();
            this.OneButton = new System.Windows.Forms.Button();
            this.ConvertButton = new System.Windows.Forms.Button();
            this.EraseButton = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            this.ResultButton = new System.Windows.Forms.Button();
            this.SumButton = new System.Windows.Forms.Button();
            this.DifferenceButton = new System.Windows.Forms.Button();
            this.MultiplyButton = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBox
            // 
            this.richTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBox.Location = new System.Drawing.Point(12, 27);
            this.richTextBox.Name = "richTextBox";
            this.richTextBox.ReadOnly = true;
            this.richTextBox.Size = new System.Drawing.Size(317, 68);
            this.richTextBox.TabIndex = 1;
            this.richTextBox.Text = "";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(343, 24);
            this.menuStrip1.TabIndex = 21;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.simpleCalculatorToolStripMenuItem,
            this.engineerCalculatorToolStripMenuItem,
            this.formulaFormToolStripMenuItem,
            this.romanCalculatorToolStripMenuItem,
            this.converterToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // simpleCalculatorToolStripMenuItem
            // 
            this.simpleCalculatorToolStripMenuItem.Name = "simpleCalculatorToolStripMenuItem";
            this.simpleCalculatorToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.simpleCalculatorToolStripMenuItem.Text = "Simple Calculator";
            this.simpleCalculatorToolStripMenuItem.Click += new System.EventHandler(this.simpleCalculatorToolStripMenuItem_Click);
            // 
            // engineerCalculatorToolStripMenuItem
            // 
            this.engineerCalculatorToolStripMenuItem.Name = "engineerCalculatorToolStripMenuItem";
            this.engineerCalculatorToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.engineerCalculatorToolStripMenuItem.Text = "Engineer Calculator";
            this.engineerCalculatorToolStripMenuItem.Click += new System.EventHandler(this.engineerCalculatorToolStripMenuItem_Click);
            // 
            // formulaFormToolStripMenuItem
            // 
            this.formulaFormToolStripMenuItem.Name = "formulaFormToolStripMenuItem";
            this.formulaFormToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.formulaFormToolStripMenuItem.Text = "Formula";
            this.formulaFormToolStripMenuItem.Click += new System.EventHandler(this.formulaToolStripMenuItem_Click);
            // 
            // romanCalculatorToolStripMenuItem
            // 
            this.romanCalculatorToolStripMenuItem.Name = "romanCalculatorToolStripMenuItem";
            this.romanCalculatorToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.romanCalculatorToolStripMenuItem.Text = "Graphica";
            this.romanCalculatorToolStripMenuItem.Click += new System.EventHandler(this.graphicaCalculatorToolStripMenuItem_Click);
            // 
            // converterToolStripMenuItem
            // 
            this.converterToolStripMenuItem.Name = "converterToolStripMenuItem";
            this.converterToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.converterToolStripMenuItem.Text = "Converter";
            this.converterToolStripMenuItem.Click += new System.EventHandler(this.converterToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.authorToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // authorToolStripMenuItem
            // 
            this.authorToolStripMenuItem.Name = "authorToolStripMenuItem";
            this.authorToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.authorToolStripMenuItem.Text = "Author";
            this.authorToolStripMenuItem.Click += new System.EventHandler(this.authorToolStripMenuItem_Click);
            // 
            // ThousandButton
            // 
            this.ThousandButton.Location = new System.Drawing.Point(174, 187);
            this.ThousandButton.Name = "ThousandButton";
            this.ThousandButton.Size = new System.Drawing.Size(75, 23);
            this.ThousandButton.TabIndex = 30;
            this.ThousandButton.Text = "M";
            this.ThousandButton.UseVisualStyleBackColor = true;
            this.ThousandButton.Click += new System.EventHandler(this.ThousandButton_Click);
            // 
            // FiveHunderButton
            // 
            this.FiveHunderButton.Location = new System.Drawing.Point(93, 187);
            this.FiveHunderButton.Name = "FiveHunderButton";
            this.FiveHunderButton.Size = new System.Drawing.Size(75, 23);
            this.FiveHunderButton.TabIndex = 29;
            this.FiveHunderButton.Text = "D";
            this.FiveHunderButton.UseVisualStyleBackColor = true;
            this.FiveHunderButton.Click += new System.EventHandler(this.FiveHunderButton_Click);
            // 
            // OneHunderButton
            // 
            this.OneHunderButton.Location = new System.Drawing.Point(12, 187);
            this.OneHunderButton.Name = "OneHunderButton";
            this.OneHunderButton.Size = new System.Drawing.Size(75, 23);
            this.OneHunderButton.TabIndex = 28;
            this.OneHunderButton.Text = "C";
            this.OneHunderButton.UseVisualStyleBackColor = true;
            this.OneHunderButton.Click += new System.EventHandler(this.OneHunderButton_Click);
            // 
            // FiftyButton
            // 
            this.FiftyButton.Location = new System.Drawing.Point(174, 158);
            this.FiftyButton.Name = "FiftyButton";
            this.FiftyButton.Size = new System.Drawing.Size(75, 23);
            this.FiftyButton.TabIndex = 27;
            this.FiftyButton.Text = "L";
            this.FiftyButton.UseVisualStyleBackColor = true;
            this.FiftyButton.Click += new System.EventHandler(this.FiftyButton_Click);
            // 
            // TenButton
            // 
            this.TenButton.Location = new System.Drawing.Point(93, 158);
            this.TenButton.Name = "TenButton";
            this.TenButton.Size = new System.Drawing.Size(75, 23);
            this.TenButton.TabIndex = 26;
            this.TenButton.Text = "X";
            this.TenButton.UseVisualStyleBackColor = true;
            this.TenButton.Click += new System.EventHandler(this.TenButton_Click);
            // 
            // FiveButton
            // 
            this.FiveButton.Location = new System.Drawing.Point(12, 158);
            this.FiveButton.Name = "FiveButton";
            this.FiveButton.Size = new System.Drawing.Size(75, 23);
            this.FiveButton.TabIndex = 25;
            this.FiveButton.Text = "V";
            this.FiveButton.UseVisualStyleBackColor = true;
            this.FiveButton.Click += new System.EventHandler(this.FiveButton_Click);
            // 
            // ThreeButton
            // 
            this.ThreeButton.Location = new System.Drawing.Point(174, 128);
            this.ThreeButton.Name = "ThreeButton";
            this.ThreeButton.Size = new System.Drawing.Size(75, 23);
            this.ThreeButton.TabIndex = 24;
            this.ThreeButton.Text = "III";
            this.ThreeButton.UseVisualStyleBackColor = true;
            this.ThreeButton.Click += new System.EventHandler(this.ThreeButton_Click);
            // 
            // TwoButton
            // 
            this.TwoButton.Location = new System.Drawing.Point(93, 128);
            this.TwoButton.Name = "TwoButton";
            this.TwoButton.Size = new System.Drawing.Size(75, 23);
            this.TwoButton.TabIndex = 23;
            this.TwoButton.Text = "II";
            this.TwoButton.UseVisualStyleBackColor = true;
            this.TwoButton.Click += new System.EventHandler(this.TwoButton_Click);
            // 
            // OneButton
            // 
            this.OneButton.Location = new System.Drawing.Point(12, 129);
            this.OneButton.Name = "OneButton";
            this.OneButton.Size = new System.Drawing.Size(75, 23);
            this.OneButton.TabIndex = 22;
            this.OneButton.Text = "I";
            this.OneButton.UseVisualStyleBackColor = true;
            this.OneButton.Click += new System.EventHandler(this.OneButton_Click);
            // 
            // ConvertButton
            // 
            this.ConvertButton.Location = new System.Drawing.Point(12, 100);
            this.ConvertButton.Name = "ConvertButton";
            this.ConvertButton.Size = new System.Drawing.Size(75, 23);
            this.ConvertButton.TabIndex = 33;
            this.ConvertButton.Text = "Convert";
            this.ConvertButton.UseVisualStyleBackColor = true;
            this.ConvertButton.Click += new System.EventHandler(this.ConvertButton_Click);
            // 
            // EraseButton
            // 
            this.EraseButton.Location = new System.Drawing.Point(173, 100);
            this.EraseButton.Name = "EraseButton";
            this.EraseButton.Size = new System.Drawing.Size(75, 23);
            this.EraseButton.TabIndex = 32;
            this.EraseButton.Text = "<-";
            this.EraseButton.UseVisualStyleBackColor = true;
            this.EraseButton.Click += new System.EventHandler(this.EraseButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(92, 100);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(75, 23);
            this.ClearButton.TabIndex = 31;
            this.ClearButton.Text = "C";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // ResultButton
            // 
            this.ResultButton.Location = new System.Drawing.Point(254, 187);
            this.ResultButton.Name = "ResultButton";
            this.ResultButton.Size = new System.Drawing.Size(75, 23);
            this.ResultButton.TabIndex = 37;
            this.ResultButton.Text = "=";
            this.ResultButton.UseVisualStyleBackColor = true;
            this.ResultButton.Click += new System.EventHandler(this.ResultButton_Click);
            // 
            // SumButton
            // 
            this.SumButton.Location = new System.Drawing.Point(254, 158);
            this.SumButton.Name = "SumButton";
            this.SumButton.Size = new System.Drawing.Size(75, 23);
            this.SumButton.TabIndex = 36;
            this.SumButton.Text = "+";
            this.SumButton.UseVisualStyleBackColor = true;
            this.SumButton.Click += new System.EventHandler(this.SumButton_Click);
            // 
            // DifferenceButton
            // 
            this.DifferenceButton.Location = new System.Drawing.Point(254, 129);
            this.DifferenceButton.Name = "DifferenceButton";
            this.DifferenceButton.Size = new System.Drawing.Size(75, 23);
            this.DifferenceButton.TabIndex = 35;
            this.DifferenceButton.Text = "-";
            this.DifferenceButton.UseVisualStyleBackColor = true;
            this.DifferenceButton.Click += new System.EventHandler(this.DifferenceButton_Click);
            // 
            // MultiplyButton
            // 
            this.MultiplyButton.Location = new System.Drawing.Point(254, 100);
            this.MultiplyButton.Name = "MultiplyButton";
            this.MultiplyButton.Size = new System.Drawing.Size(75, 23);
            this.MultiplyButton.TabIndex = 34;
            this.MultiplyButton.Text = "X";
            this.MultiplyButton.UseVisualStyleBackColor = true;
            this.MultiplyButton.Click += new System.EventHandler(this.MultiplyButton_Click);
            // 
            // RomanCalcForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(343, 221);
            this.Controls.Add(this.ResultButton);
            this.Controls.Add(this.SumButton);
            this.Controls.Add(this.DifferenceButton);
            this.Controls.Add(this.MultiplyButton);
            this.Controls.Add(this.ConvertButton);
            this.Controls.Add(this.EraseButton);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.ThousandButton);
            this.Controls.Add(this.FiveHunderButton);
            this.Controls.Add(this.OneHunderButton);
            this.Controls.Add(this.FiftyButton);
            this.Controls.Add(this.TenButton);
            this.Controls.Add(this.FiveButton);
            this.Controls.Add(this.ThreeButton);
            this.Controls.Add(this.TwoButton);
            this.Controls.Add(this.OneButton);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.richTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "RomanCalcForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RomanCalcForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.romanCalcForm_Closing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem simpleCalculatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem engineerCalculatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Button ThousandButton;
        private System.Windows.Forms.Button FiveHunderButton;
        private System.Windows.Forms.Button OneHunderButton;
        private System.Windows.Forms.Button FiftyButton;
        private System.Windows.Forms.Button TenButton;
        private System.Windows.Forms.Button FiveButton;
        private System.Windows.Forms.Button ThreeButton;
        private System.Windows.Forms.Button TwoButton;
        private System.Windows.Forms.Button OneButton;
        private System.Windows.Forms.Button ConvertButton;
        private System.Windows.Forms.Button EraseButton;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.Button ResultButton;
        private System.Windows.Forms.Button SumButton;
        private System.Windows.Forms.Button DifferenceButton;
        private System.Windows.Forms.Button MultiplyButton;
        private System.Windows.Forms.ToolStripMenuItem formulaFormToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem authorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem romanCalculatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem converterToolStripMenuItem;
    }
}