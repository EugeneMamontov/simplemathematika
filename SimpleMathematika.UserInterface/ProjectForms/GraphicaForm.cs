﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using System;
using System.Windows.Forms;
/*****************************************************************************/

namespace SimpleMathematika.UserInterface.ProjectForms
{
    public partial class GraphicaForm : Form
    {
        public int K { get; private set; }

/*---------------------------------------------------------------------------*/

        public GraphicaForm ()
        {
            InitializeComponent();
            K = 0;
        }

/*---------------------------------------------------------------------------*/

        public void graphicaForm_Closing (
                object sender
            ,   FormClosingEventArgs e
        )
        {
            Application.Exit();
        }

/*---------------------------------------------------------------------------*/

        private void simpleCalculatorToolStripMenuItem_Click (
                object sender
            ,   EventArgs e
        )
        {
            var SCF = new SimpleCalcForm();
            SCF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void engineerCalculatorToolStripMenuItem_Click (
                object sender
            ,   EventArgs e
        )
        {
            var ECF = new EngineerCalcForm();
            ECF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void romanCalculatorToolStripMenuItem_Click (
                object sender
            ,   EventArgs e
        )
        {
            var RCF = new RomanCalcForm();
            RCF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void formulaToolStripMenuItem_Click ( 
                object sender
            ,   EventArgs e
        )
        {
            var GF = new FormulaForm();
            GF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void converterToolStripMenuItem_Click ( 
                object sender
            ,   EventArgs e
        )
        {
            var C = new ConverterForm();
            C.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void authorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show( 
                    "Invented by Yevheii Mamontov, NURE 2017." 
                +   "\nFor contact use yevhenii.mamontov@nure.ua" 
            );
        }

/*---------------------------------------------------------------------------*/

        private void SinButton_Click ( object sender, EventArgs e )
        {
            clear();
            enableScalability( 20 );
            handleK();

            for ( int i = 0; i < 20; ++i )
                if ( K != 0 )
                    chart.Series[ 0 ].Points.AddXY( i, Math.Sin( K * i ) );
                else
                    chart.Series[ 0 ].Points.AddXY( i, Math.Sin( i ) );
        }

/*---------------------------------------------------------------------------*/

        private void CosButton_Click ( object sender, EventArgs e )
        {
            clear();
            enableScalability( 20 );
            handleK();

            for ( int i = 0; i < 20; ++i )
                if ( K != 0 )
                    chart.Series[ 1 ].Points.AddXY( i, Math.Cos( K * i ) );
                else
                    chart.Series[ 1 ].Points.AddXY( i, Math.Cos( i ) );
        }

/*---------------------------------------------------------------------------*/

        private void TanButton_Click ( object sender, EventArgs e )
        {
            clear();
            enableScalability( 10 );
            handleK();

            for ( int i = 0; i < 10; ++i )
                if ( K != 0 )
                    chart.Series[ 2 ].Points.AddXY( i, Math.Tan( K * i ) );
                else
                    chart.Series[ 2 ].Points.AddXY( i, Math.Tan( i ) );
        }

/*---------------------------------------------------------------------------*/

        private void SinHButton_Click ( object sender, EventArgs e )
        {
            clear();
            enableScalability( 20 );
            handleK();

            for ( int i = 0; i < 20; ++i )
                if ( K != 0 )
                    chart.Series[ 3 ].Points.AddXY( i, Math.Sinh( K * i ) );
                else
                    chart.Series[ 3 ].Points.AddXY( i, Math.Sinh( i ) );
        }

/*---------------------------------------------------------------------------*/

        private void CosHButton_Click ( object sender, EventArgs e )
        {
            clear();
            enableScalability( 20 );
            handleK();

            for ( int i = 0; i < 20; ++i )
                if ( K != 0 )
                    chart.Series[ 4 ].Points.AddXY( i, Math.Cosh( K * i ) );
                else
                    chart.Series[ 4 ].Points.AddXY( i, Math.Cosh( i ) );
        }

/*---------------------------------------------------------------------------*/

        private void TanHButton_Click ( object sender, EventArgs e )
        {
            clear();
            enableScalability( 10 );
            handleK();

            for ( int i = 0; i < 10; ++i )
                if ( K != 0 )
                    chart.Series[ 5 ].Points.AddXY( i, Math.Tanh( K * i ) );
                else
                    chart.Series[ 5 ].Points.AddXY( i, Math.Tanh( i ) );
        }

/*---------------------------------------------------------------------------*/

        private void LinearGraphButton_Click ( object sender, EventArgs e )
        {
            clear();
            enableScalability( 100 );
            handleK();

            for ( int i = 0; i < 10; ++i )
                if ( K != 0 )
                    chart.Series[ 6 ].Points.AddXY( i, i * K );
                else
                    chart.Series[ 6 ].Points.AddXY( i, i );      
        }

/*---------------------------------------------------------------------------*/

        private void SquardGraphButton_Click ( object sender, EventArgs e )
        {
            clear();
            enableScalability( 10 );
            handleK();

            for ( int i = 0; i < 10; ++i )
                chart.Series[ 7 ].Points.AddXY( i, i * i );
        }

/*---------------------------------------------------------------------------*/

        private void ClearButton_Click ( object sender, EventArgs e )
        {
            clear();
        }

/*---------------------------------------------------------------------------*/

        private void clear ()
        {
            for ( int i = 0; i < chart.Series.Count; ++i )
                chart.Series[ i ].Points.Clear();
        }

/*---------------------------------------------------------------------------*/

        private void enableScalability ( int _rightSideInterval )
        {
            chart.ChartAreas[ 0 ].AxisX.ScaleView.Zoom( 0, _rightSideInterval );
            chart.ChartAreas[ 0 ].CursorX.IsUserEnabled = true;
            chart.ChartAreas[ 0 ].CursorX.IsUserSelectionEnabled = true;
            chart.ChartAreas[ 0 ].AxisX.ScaleView.Zoomable = true;
            chart.ChartAreas[ 0 ].AxisX.ScrollBar.IsPositionedInside = true;
        }

/*---------------------------------------------------------------------------*/

        private void handleK ()
        {
            if ( textBox1.Text.Length != 0 )
                K = Convert.ToInt32( textBox1.Text );
        }
    }
}


/*****************************************************************************/