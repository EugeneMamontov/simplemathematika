﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using System;
using System.Windows.Forms;
using System.Collections.Generic;
using SimpleMathematika.Core.Engines;
/*****************************************************************************/


namespace SimpleMathematika.UserInterface.ProjectForms
{
    public partial class ConverterForm : Form
    {
        public ConvectorEngine CE       { get; private set; }
        public List< string > Buffer    { get; set; }

/*---------------------------------------------------------------------------*/

        public ConverterForm ()
        {
            CE = new ConvectorEngine();
            InitializeComponent();
        }

/*---------------------------------------------------------------------------*/

        public void converterForm_Closing (
                object sender
            ,   FormClosingEventArgs e
        )
        {
            Application.Exit();
        }

/*---------------------------------------------------------------------------*/

        private void simpleCalculatorToolStripMenuItem_Click (
                object sender
            ,   EventArgs e
        )
        {
            var SCF = new SimpleCalcForm();
            SCF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void engineerCalculatorToolStripMenuItem_Click (
                object sender
            ,   EventArgs e
        )
        {
            var ECF = new EngineerCalcForm();
            ECF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void romanCalculatorToolStripMenuItem1_Click (
                object sender
            ,   EventArgs e
        )
        {
            var RCF = new RomanCalcForm();
            RCF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void formulaToolStripMenuItem_Click ( 
                object sender
            ,   EventArgs e
        )
        {
            var F = new FormulaForm();
            F.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/

        private void graphicaToolStripMenuItem_Click ( 
                object sender
            ,   EventArgs e
        )
        {
            var GF = new GraphicaForm();
            GF.Show();
            this.Hide();
        }

/*---------------------------------------------------------------------------*/
        
        private void authorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show( 
                    "Invented by Yevheii Mamontov, NURE 2017." 
                +   "\nFor contact use yevhenii.mamontov@nure.ua" 
            );
        }

/*---------------------------------------------------------------------------*/

        private void ClearButton_Click ( object sender, EventArgs e )
        {
            richTextBox.Clear();
        }

/*---------------------------------------------------------------------------*/

        private void HexButton_Click ( object sender, EventArgs e )
        {
            try
            { 
                int value = Convert.ToInt32( richTextBox.Text );
                richTextBox.Text 
                    += " converted to " + CE.convertFromIntToHex( ref value );
            }
            catch ( Exception _ex )
            {
                richTextBox.Clear();
            }
        }

/*---------------------------------------------------------------------------*/

        private void OctalButton_Click ( object sender, EventArgs e )
        {
            try
            { 
                int value = Convert.ToInt32( richTextBox.Text );
                richTextBox.Text 
                    += " converted to " + CE.convertFromIntToOctal( ref value );
            }
            catch ( Exception _ex )
            {
                richTextBox.Clear();
            }
        }

/*---------------------------------------------------------------------------*/

        private void BinaryButton_Click ( object sender, EventArgs e )
        {
            try
            { 
                int value = Convert.ToInt32( richTextBox.Text );
                richTextBox.Text 
                    += " converted to " + CE.convertFromIntToBinary( ref value );
            }
            catch ( Exception _ex )
            {
                richTextBox.Clear();
            }
        }

/*---------------------------------------------------------------------------*/

        private void IntButton_Click ( object sender, EventArgs e )
        {
            try
            {
                richTextBox.Text += " converted to " + richTextBox.Text;
            }
            catch ( Exception _ex )
            {
                richTextBox.Clear();
            }
        }
    }
}


/*****************************************************************************/