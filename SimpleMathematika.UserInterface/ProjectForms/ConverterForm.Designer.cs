﻿namespace SimpleMathematika.UserInterface.ProjectForms
{
    partial class ConverterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox = new System.Windows.Forms.RichTextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.simpleCalculatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.romanCalculatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.romanCalculatorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.formulaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.graphicaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.authorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ClearButton = new System.Windows.Forms.Button();
            this.HexButton = new System.Windows.Forms.Button();
            this.OctalButton = new System.Windows.Forms.Button();
            this.BinaryButton = new System.Windows.Forms.Button();
            this.IntButton = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBox
            // 
            this.richTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBox.Location = new System.Drawing.Point(12, 30);
            this.richTextBox.Name = "richTextBox";
            this.richTextBox.Size = new System.Drawing.Size(318, 100);
            this.richTextBox.TabIndex = 3;
            this.richTextBox.Text = "";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(342, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.simpleCalculatorToolStripMenuItem,
            this.romanCalculatorToolStripMenuItem,
            this.romanCalculatorToolStripMenuItem1,
            this.formulaToolStripMenuItem,
            this.graphicaToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // simpleCalculatorToolStripMenuItem
            // 
            this.simpleCalculatorToolStripMenuItem.Name = "simpleCalculatorToolStripMenuItem";
            this.simpleCalculatorToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.simpleCalculatorToolStripMenuItem.Text = "Simple Calculator";
            this.simpleCalculatorToolStripMenuItem.Click += new System.EventHandler(this.simpleCalculatorToolStripMenuItem_Click);
            // 
            // romanCalculatorToolStripMenuItem
            // 
            this.romanCalculatorToolStripMenuItem.Name = "romanCalculatorToolStripMenuItem";
            this.romanCalculatorToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.romanCalculatorToolStripMenuItem.Text = "Engineer Calculator";
            this.romanCalculatorToolStripMenuItem.Click += new System.EventHandler(this.engineerCalculatorToolStripMenuItem_Click);
            // 
            // romanCalculatorToolStripMenuItem1
            // 
            this.romanCalculatorToolStripMenuItem1.Name = "romanCalculatorToolStripMenuItem1";
            this.romanCalculatorToolStripMenuItem1.Size = new System.Drawing.Size(177, 22);
            this.romanCalculatorToolStripMenuItem1.Text = "Roman Calculator";
            this.romanCalculatorToolStripMenuItem1.Click += new System.EventHandler(this.romanCalculatorToolStripMenuItem1_Click);
            // 
            // formulaToolStripMenuItem
            // 
            this.formulaToolStripMenuItem.Name = "formulaToolStripMenuItem";
            this.formulaToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.formulaToolStripMenuItem.Text = "Formula";
            this.formulaToolStripMenuItem.Click += new System.EventHandler(this.formulaToolStripMenuItem_Click);
            // 
            // graphicaToolStripMenuItem
            // 
            this.graphicaToolStripMenuItem.Name = "graphicaToolStripMenuItem";
            this.graphicaToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.graphicaToolStripMenuItem.Text = "Graphica";
            this.graphicaToolStripMenuItem.Click += new System.EventHandler(this.graphicaToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.authorToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // authorToolStripMenuItem
            // 
            this.authorToolStripMenuItem.Name = "authorToolStripMenuItem";
            this.authorToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.authorToolStripMenuItem.Text = "Author";
            this.authorToolStripMenuItem.Click += new System.EventHandler(this.authorToolStripMenuItem_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(255, 136);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(75, 23);
            this.ClearButton.TabIndex = 43;
            this.ClearButton.Text = "Clear";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // HexButton
            // 
            this.HexButton.Location = new System.Drawing.Point(12, 165);
            this.HexButton.Name = "HexButton";
            this.HexButton.Size = new System.Drawing.Size(75, 23);
            this.HexButton.TabIndex = 44;
            this.HexButton.Text = "toHex";
            this.HexButton.UseVisualStyleBackColor = true;
            this.HexButton.Click += new System.EventHandler(this.HexButton_Click);
            // 
            // OctalButton
            // 
            this.OctalButton.Location = new System.Drawing.Point(93, 165);
            this.OctalButton.Name = "OctalButton";
            this.OctalButton.Size = new System.Drawing.Size(75, 23);
            this.OctalButton.TabIndex = 45;
            this.OctalButton.Text = "toOctal";
            this.OctalButton.UseVisualStyleBackColor = true;
            this.OctalButton.Click += new System.EventHandler(this.OctalButton_Click);
            // 
            // BinaryButton
            // 
            this.BinaryButton.Location = new System.Drawing.Point(174, 165);
            this.BinaryButton.Name = "BinaryButton";
            this.BinaryButton.Size = new System.Drawing.Size(75, 23);
            this.BinaryButton.TabIndex = 46;
            this.BinaryButton.Text = "toBinary";
            this.BinaryButton.UseVisualStyleBackColor = true;
            this.BinaryButton.Click += new System.EventHandler(this.BinaryButton_Click);
            // 
            // IntButton
            // 
            this.IntButton.Location = new System.Drawing.Point(255, 165);
            this.IntButton.Name = "IntButton";
            this.IntButton.Size = new System.Drawing.Size(75, 23);
            this.IntButton.TabIndex = 47;
            this.IntButton.Text = "toInt";
            this.IntButton.UseVisualStyleBackColor = true;
            this.IntButton.Click += new System.EventHandler(this.IntButton_Click);
            // 
            // ConverterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 200);
            this.Controls.Add(this.IntButton);
            this.Controls.Add(this.BinaryButton);
            this.Controls.Add(this.OctalButton);
            this.Controls.Add(this.HexButton);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.richTextBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ConverterForm";
            this.Text = "ConverterForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.converterForm_Closing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem simpleCalculatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem romanCalculatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem romanCalculatorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem formulaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem graphicaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem authorToolStripMenuItem;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.Button HexButton;
        private System.Windows.Forms.Button OctalButton;
        private System.Windows.Forms.Button BinaryButton;
        private System.Windows.Forms.Button IntButton;
    }
}