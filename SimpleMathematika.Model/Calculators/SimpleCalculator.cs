﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using SimpleMathematika.Utilites.Values;
/*****************************************************************************/


namespace SimpleMathematika.Model.Calculators
{
    public class SimpleCalculator : Calculator
    {
        public SimpleCalculator () { }

/*---------------------------------------------------------------------------*/

        public SimpleCalculator (
                MathValue _firstOperand
            ,   MathValue _secondOperand 
        )
            :   base ( _firstOperand, _secondOperand )
        {
        }

/*---------------------------------------------------------------------------*/

        public void evaluateSum ()
        {
            Result = FirstOperand + SecondOperand;
        }

/*---------------------------------------------------------------------------*/

        public void evaluateDifference ()
        {
            Result = FirstOperand - SecondOperand;
        }

/*---------------------------------------------------------------------------*/

        public void evaluateMultiplication ()
        {
            Result = FirstOperand * SecondOperand;
        }

/*---------------------------------------------------------------------------*/

        public void evaluateDivision ()
        {
            Result = FirstOperand / SecondOperand;
        }
    }
}


/*****************************************************************************/