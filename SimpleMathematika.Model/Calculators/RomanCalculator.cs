﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using SimpleMathematika.Core.Engines;
using SimpleMathematika.Utilites.Values;
/*****************************************************************************/


namespace SimpleMathematika.Model.Calculators
{
    public class RomanCalculator : Calculator
    {
        public RomanCalculatorEngine Engine { get; }
        public RomanValueResult Result { get; set; }
        public string Data { get; set; }

/*---------------------------------------------------------------------------*/

        public RomanCalculator ()
        {
            Data = "";
            Result = new RomanValueResult( 0.0 );
            Engine = new RomanCalculatorEngine();
        }

/*---------------------------------------------------------------------------*/

        public RomanCalculator ( string _data )
        {
            Data   = _data;
            Result = new RomanValueResult( 0.0 );
            Engine = new RomanCalculatorEngine();
        }

/*---------------------------------------------------------------------------*/

        public void convertToArabianValue ()
        {
            double arabianValue = Engine.convertToArabianValue( Data );
            Result = new RomanValueResult( arabianValue );
        }

/*---------------------------------------------------------------------------*/

        public string convertToRomanValue ()
        {
            return Engine.convertToRomanValue( Result );
        }

/*---------------------------------------------------------------------------*/

        public bool isCorrectRepeatedDivisibleByTen ()
        {
            return Engine.isCorrectRepeatedDivisibleByTen( Data );
        }

/*---------------------------------------------------------------------------*/

        public bool isRepeatedDivisibleByFive ()
        {
            return Engine.isRepeatedDivisibleByFive( Data );
        }
    }
}


/*****************************************************************************/