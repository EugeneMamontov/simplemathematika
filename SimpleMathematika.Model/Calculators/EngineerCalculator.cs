﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using System;
using SimpleMathematika.Core.Engines;
using SimpleMathematika.Utilites.Values;
/*****************************************************************************/


namespace SimpleMathematika.Model.Calculators
{
    public class EngineerCalculator : SimpleCalculator
    {
        public MathValue Angle { get; set; }
        public ConvectorEngine Engine { get; set; }

/*---------------------------------------------------------------------------*/

        public EngineerCalculator ()
        {
            Angle = new MathValue( 0.0 );
            Engine = new ConvectorEngine();
        }

/*---------------------------------------------------------------------------*/

        public EngineerCalculator (
                MathValue _firstOperand
            ,   MathValue _secondOperand
        )
            :   base ( _firstOperand, _secondOperand )
        {
            Angle = new MathValue( 0.0 );
        }

/*---------------------------------------------------------------------------*/

        public void evaluateSin ()
        {
            Result = new MathValue( Math.Sin( Angle.Data ) );
        }

/*---------------------------------------------------------------------------*/

        public void evaluateCos ()
        {
            Result = new MathValue( Math.Cos( Angle.Data ) );
        }

/*---------------------------------------------------------------------------*/

        public void evaluateTan ()
        {
            Result = new MathValue( Math.Cos( Angle.Data ) );
        }

/*---------------------------------------------------------------------------*/

        public void evaluatePower ()
        {
            Result = new MathValue ( 
                Math.Pow( FirstOperand.Data, SecondOperand.Data ) 
            );
        }

/*---------------------------------------------------------------------------*/

        public void evaluatePower2 ()
        {
            Result = new MathValue ( 
                Math.Pow( FirstOperand.Data, 2 ) 
            );
        }

/*---------------------------------------------------------------------------*/

        public void evaluatePow ()
        {
            // NOTE: Takes n times sqrt from a value.

            Result = new MathValue (
                Math.Pow( FirstOperand.Data, 1.0 / SecondOperand.Data ) 
            );
        }

/*---------------------------------------------------------------------------*/

        public void evaluatePow2 ()
        {
            Result = new MathValue (
                Math.Pow( FirstOperand.Data, 1.0 / 2 ) 
            );
        }

/*---------------------------------------------------------------------------*/
        
        public void evaluateLog ()
        {
            Result = new MathValue( Math.Log( FirstOperand.Data ) );
        }
    }
}


/*****************************************************************************/