﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using SimpleMathematika.Utilites.Values;
/*****************************************************************************/


namespace SimpleMathematika.Model.Calculators
{
    public abstract class Calculator
    {
        public MathValue FirstOperand   { get; set; }
        public MathValue SecondOperand  { get; set; }
        public MathValue Result         { get; set; }

/*---------------------------------------------------------------------------*/

        protected Calculator ()
        {
            FirstOperand    = new MathValue ( 0.0 );
            SecondOperand   = new MathValue ( 0.0 );
            Result          = new MathValue ( 0.0 );
        }

/*---------------------------------------------------------------------------*/

        protected Calculator (
                MathValue _firstOperand
            ,   MathValue _secondOperand 
        )
        {
            FirstOperand    = new MathValue ( _firstOperand.Data );
            SecondOperand   = new MathValue ( _secondOperand.Data );
            Result          = new MathValue ( 0.0 );
        }
    }
}


/*****************************************************************************/