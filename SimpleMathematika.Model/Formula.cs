﻿/** (C) Yevhenii Mamontov, NURE 2017 */

/*****************************************************************************/
using SimpleMathematika.Core.FormulaParser;
using SimpleMathematika.Utilites.Values;
/*****************************************************************************/


namespace SimpleMathematika.Model
{
    public class Formula
    {
        public FormulaParser FormulaParser  { get; private set; }
        public MathValue Result             { get; set; }
        public string StringToParse         { get; set; }

/*---------------------------------------------------------------------------*/

        public Formula ()
        {
            FormulaParser = new FormulaParser();
        }

/*---------------------------------------------------------------------------*/

        public void tryParse ()
        {
            try
            {
                Result = new MathValue( FormulaParser.parse( StringToParse ) );
            }
            catch ( System.Exception _ex )
            {
                System.Console.WriteLine( "Can't parse. " + _ex.Message );
                Result = new MathValue( -1.0 );
            }
        }

/*---------------------------------------------------------------------------*/

        public EquatationRoots evaluateSquardEquatation ()
        {
            return FormulaParser.evaluateSquardEquatation( StringToParse );
        }

/*---------------------------------------------------------------------------*/

        public void addExpressionMember ( string _name, double _value )
        {
            FormulaParser.addExpressionMember( _name, _value );
        }
    }
}


/*****************************************************************************/